//
//  NSMutableAttributedString+GoodStuff.h
//  SimplyStats
//
//  Created by Rob van der Veer on 17/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 Helpers functions for working with Attributed Strings
 */
@interface NSMutableAttributedString (GoodStuff)

/**
 * Sets or gets the default font that will be used by append methods
 */
@property (nonatomic, weak) UIFont *defaultFont;

/**
 * Sets or gets the default font that will be used by appendBold methods
 */
@property (nonatomic, weak) UIFont *boldFont;

/**
 * Appends a string with the default font defaultFont
 * @param string The string to add
 */
-(void)append:(NSString *)string;

/**
 * Appends a string with a specific font
 * @param string The string to add
 * @param font The font to use
 */
-(void)append:(NSString *)string withFont:(UIFont *)font;

/**
 * Appends a string with a specific single style
 *
 * When adding multiple style, see appendAttributedString where you can specify multiple attributes.
 * @param string The string to add
 * @param style The style to set
 * @param value The value for the style
 */
-(void)append:(NSString *)string withStyle:(NSString *)style value:(id)value;

/**
 * Append a string using the boldFont font
 * @param string The string to append
 */
-(void)appendBold:(NSString *)string;

/**
 * Append a single integer with the bold font
 *
 * The number is formatted with a simple '%d'. Well at least it saves you a stringWithNumber.
 * @param number The number to append
 */
-(void)appendBoldInteger:(int)number;
@end
