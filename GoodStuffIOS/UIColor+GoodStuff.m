//
//  UIColor+additions.m
//  SimplyStats
//
//  Created by Rob van der Veer on 11/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "UIColor+GoodStuff.h"

@implementation UIColor (GoodStuff)

- (UIColor *)blendWith:(UIColor *)other mix:(CGFloat)percentage
{
    percentage = MAX(0,MIN(1, percentage));
    
    CGFloat red1, green1, blue1, alpha1;
    CGFloat red2, green2, blue2, alpha2;
    
    [self getRed:&red1 green:&green1 blue:&blue1 alpha:&alpha1];
    [other getRed:&red2 green:&green2 blue:&blue2 alpha:&alpha2];
    
    CGFloat red = (red1 * (1-percentage) + red2 * percentage);
    CGFloat green = (green1 * (1-percentage) + green2 * percentage);
    CGFloat blue = (blue1 * (1-percentage) + blue2 * percentage);
    CGFloat alpha = 1; //(alpha1 * (1-percentage) + alpha2 * percentage);
    
    UIColor *result = [[UIColor alloc] initWithRed:red green:green blue:blue alpha:alpha];
    return result;
}

- (UIColor *)getContrastColor
{
    CGFloat hue;
    CGFloat saturation;
    CGFloat brightness;
    CGFloat alpha;
    
    [self getHue:&hue saturation:&saturation brightness:&brightness alpha:&alpha];
   
    hue += 0.5;
    if(hue > 1)
    {
        hue -= 1;
    }
    
    UIColor *result =
    [[UIColor alloc] initWithHue:hue saturation:saturation brightness:brightness alpha:alpha];
    return result;
}

-(UIColor *)getInverseColor
{
    CGFloat r,g,b,a;
    [self getRed:&r green:&g blue:&b alpha:&a];
    return [UIColor colorWithRed:1.-r green:1.-g blue:1.-b alpha:a];
}


- (UIColor *)getBrighter
{
    CGFloat hue;
    CGFloat saturation;
    CGFloat brightness;
    CGFloat alpha;
    
    [self getHue:&hue saturation:&saturation brightness:&brightness alpha:&alpha];
    brightness = MIN(1, brightness * 2);
    
    UIColor *result =
    [[UIColor alloc] initWithHue:hue saturation:saturation brightness:brightness alpha:alpha];
    return result;
}

- (UIColor *)getDarker
{
    CGFloat hue;
    CGFloat saturation;
    CGFloat brightness;
    CGFloat alpha;
    
    [self getHue:&hue saturation:&saturation brightness:&brightness alpha:&alpha];
    brightness *= 0.5;
    UIColor *result =
    [[UIColor alloc] initWithHue:hue saturation:saturation brightness:brightness alpha:alpha];
    return result;
}


- (UIImage *)getSolidImage
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 16.0f, 16.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [self CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

-(NSString *)debugValueString
{
    CGFloat r,g,b,a;
    [self getRed:&r green:&g blue:&b alpha:&a];
    
    return [NSString stringWithFormat:@"[%d,%d,%d,%d]", (int)(r*255), (int)(g*255),(int)(b*255),(int)(a*255)];
}

+(UIColor *)colorFromHexString:(NSString *)hex
{
    //NSAssert(hex.length == 6, @"Hex string must be 6 bytes RRGGBB");
    
    if(hex.length < 6)
    {
        return nil;
    }
    
    unsigned int c;
    if ([hex characterAtIndex:0] == '#') {
        [[NSScanner scannerWithString:[hex substringFromIndex:1]] scanHexInt:&c];
    } else {
        [[NSScanner scannerWithString:hex] scanHexInt:&c];
    }
    return [UIColor colorWithRed:((c & 0xff0000) >> 16)/255.0
                           green:((c & 0xff00) >> 8)/255.0
                            blue:(c & 0xff)/255.0 alpha:1.0];
    
}
@end
