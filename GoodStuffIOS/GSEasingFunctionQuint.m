//
//  GSEasingFunctionQuint.m
//  GoodStuffIOS
//
//  Created by Rob van der Veer on 14/7/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "GSEasingFunctionQuint.h"

@implementation GSEasingFunctionQuint
-(CGFloat)easeCore:(CGFloat)t
{
    return t * t * t * t * t;
}
@end
