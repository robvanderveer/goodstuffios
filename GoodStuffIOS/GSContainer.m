//
//  GSContainer.m
//  GoodStuffIOS
//
//  Created by Rob van der Veer on 28/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "GSContainer.h"
#import <objc/runtime.h>

@interface GSContainerItem : NSObject
@property (nonatomic, copy) GSConstructionBlock constructor;
@end

@implementation GSContainerItem
@end

@interface GSContainer ()
@property (nonatomic, strong) NSMutableDictionary *constructors;

@end

@implementation GSContainer
-(id)init
{
    self = [super init];
    if(self)
    {
        _constructors = [[NSMutableDictionary alloc] init];
    }
    return self;
}

-(void)setup:(Protocol *)protocol named:(NSString *)name with:(GSConstructionBlock)constructor
{
    //get the full name of the interface.
    NSString *key = [self getKey:protocol andName:name];
    GSContainerItem *entry = [_constructors objectForKey:key];
    if(entry)
    {
        [NSException raise:@"Key already registered" format:@"%@", key];
    }
    
    entry = [[GSContainerItem alloc] init];
    entry.constructor = constructor;
    _constructors[key] = entry;
}

-(id)obtain:(Protocol *)protocol named:(NSString *)name
{
    //get the full name of the interface.
    NSString *key = [self getKey:protocol andName:name];
    GSContainerItem *entry = [_constructors objectForKey:key];
    if(!entry)
    {
        [NSException raise:@"Key not registered" format:@"%@", key];
    }
    
    id object = entry.constructor(self);
    
    //make sure the constructed object understands the protocol.
    bool support = [object conformsToProtocol:protocol];
    if(!support)
    {
        [NSException raise:@"Constructed object does not conform to protocol" format:@"%@", [self getProtocolName:protocol]];
    }
    return object;
}

-(void)setup:(Protocol *)protocol with:(GSConstructionBlock)constructor
{
    [self setup:protocol named:@"default" with:constructor];
}

-(id)obtain:(Protocol *)protocol
{
    return [self obtain:protocol named:@"default"];
}

-(NSString *)getKey:(Protocol *)p andName:(NSString *)name
{
    NSString *key = [NSString stringWithFormat:@"%@_|_%@", [self getProtocolName:p], name];
    return key;
}

-(NSString *)getProtocolName:(Protocol *)p
{
    const char *protocolName = protocol_getName(p);
    return [NSString stringWithCString:protocolName encoding:NSUTF8StringEncoding];
}
@end
