//
//  GSEasingFunctionLinear.h
//  GoodStuffIOS
//
//  Created by Rob van der Veer on 14/7/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "GSEasingFunction.h"

/**
 A linear easing function. It maps normalized time to itself. Specifying EaseIn, EaseOut or EaseInOut has little use as there is no curve.
 
 ![easeIn](../docs/Images/curveEasingFunctionLinear_easeIn.png =200x150)
 ![easeOut](../docs/Images/curveEasingFunctionLinear_easeOut.png =200x150)
 ![easeInOut](../docs/Images/curveEasingFunctionLinear_easeInOut.png =200x150)
 
 see GSEasingFunction
 
 */
@interface GSEasingFunctionLinear : GSEasingFunction

@end
