//
//  RVDatePicker.h
//  CalendarDemo
//
//  Created by Rob van der Veer on 15/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GSCalendarView.h"

/** Specifies the current displaymode for a GSDatePicker */
typedef NS_ENUM(NSUInteger, GSCalendarMode)
{
    /** shows the days of a month */
    SelectDate,
    
    /** shows the months of a year */
    SelectMonth,
    
    /** shows the years */
    SelectYear
};

@class GSDatePicker;

/** A delegate used by the GSDatePicker class */
@protocol GSDatePickerDelegate <NSObject>

/** Called when the user changed the selected date
 * @param sender The GSDatePicker for which the selected date was changed 
 * @param date The newly selected date
 */
-(void)didChangeSelection:(GSDatePicker *)sender newDate:(NSDate *)date;
@end

/** The date picker view will encapsulate everything you need to select a date 
 *
 * Starting out with a day grid for the current date, the user can click on any date to
 * select one. By using the previous and next buttons, a different month can be selected.
 * Clicking on a month name, brings up a quick selector for the month. Clicking on the 
 * Year label that appears, the user can quickly skip through the years.
 *
 * A delegate is provided to handle selection of a new date, e.g. closing a picker popup.
 */
@interface GSDatePicker : UIView<GSCalendarViewDelegate>

/** The current state of the date picker 
 * @see GSCalendarMode
 */
@property (nonatomic) GSCalendarMode mode;

/** Specifies the currently selected date */
@property (nonatomic, strong) NSDate *selectedDate;

/** Visual appearance settings
 *
 * @see GSVisualPreferences
 */@property (nonatomic, strong) GSVisualPreferences *appearance;

/** Specifies the calendar to be used. Defaults to the current locale calendar. */
@property (nonatomic, strong) NSCalendar *calendar;

/** Specifies the maximum selectable date */
@property (nonatomic, strong) NSDate *maximumDate;

/** Specifies the earliest date that can be selected */
@property (nonatomic, strong) NSDate *minimumDate;

/** A delegate that can be used to handle new date selection */
@property (nonatomic, strong) id<GSDatePickerDelegate> delegate;
@end
