//
//  NSArray+GoodStuff.m
//  GoodStuffIOS
//
//  Created by Rob van der Veer on 5/7/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "NSArray+GoodStuff.h"

@implementation NSArray (GoodStuff)
-(NSArray *)select:(id (^)(id))function
{
    NSMutableArray *result = [[NSMutableArray alloc] initWithCapacity:self.count];
    for(id item in self)
    {
        id newItem = function(item);
        [result addObject:newItem];
    }
    return result;
}

-(NSArray *)ofClass:(Class)class
{
    return [self where:^bool(id currentItem) {
        return [currentItem isKindOfClass:class];
    }];
}

-(NSArray *)where:(bool (^)(id item))predicate
{
    NSMutableArray *result = [[NSMutableArray alloc] initWithCapacity:self.count];
    for(id item in self)
    {
        if(predicate(item))
        {
            [result addObject:item];
        }
    }
    return result;
}

+(NSArray *)range:(NSInteger)from to:(NSInteger)to with:(id (^)(NSInteger))function
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    for(NSInteger val = from; val <= to; val++)
    {
         [result addObject:function(val)];
    }
    return result;
}

-(NSArray *)skip:(NSInteger)number
{
    NSMutableArray *result = [[NSMutableArray alloc] initWithCapacity:self.count-number];
    NSUInteger skipCount = number;
    
    for(id item in self)
    {
        if(skipCount > 0)
        {
            skipCount--;
        }
        else
        {
            [result addObject:item];
        }
    }
    return result;
}


-(NSArray *)take:(NSInteger)number
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    int taken = 0;
    
    for(id item in self)
    {
        if(taken >= number)
        {
            break;
        }
        [result addObject:item];
        taken++;
    }

    return result;
}

-(id)firstOrDefault
{
    if(self.count >= 1)
    {
        return [self objectAtIndex:0];
    }
    else
    {
        return nil;
    }
}

-(id)singleOrDefault
{
    if(self.count > 1)
    {
        @throw [[NSException alloc] initWithName:NSInvalidArgumentException reason:@"collection has more than one element" userInfo:nil];
    }
    else
    {
        return [self firstOrDefault];
    }
}

-(id)firstOrDefault:(bool (^)(id item))predicate
{
    for(id item in self)
    {
        if(predicate(item))
        {
            return item;
        }
    }
    return NULL;
}


-(id)lastOrDefault
{
    return [self lastOrDefault:^bool(id currentItem) {
        return true;
    }];
}

-(id)lastOrDefault:(bool (^)(id item))predicate
{
    id candidateItem = NULL;
    
    for(id item in self)
    {
        if(predicate(item))
        {
            candidateItem = item;
        }
    }
    return candidateItem;
}

-(bool)any:(bool (^)(id item))predicate
{
    return ([self firstOrDefault:predicate])?true:false;
}

-(bool)all:(bool (^)(id item))predicate
{
    for(id item in self)
    {
        if(!predicate(item))
        {
            return false;
        }
    }
    return true;
}

-(CGFloat)sum:(CGFloat(^)(id item))predicate
{
    CGFloat total = 0;
    
    for(id item in self)
    {
        total += predicate(item);
    }
    return total;
}

-(CGFloat)max:(CGFloat(^)(id item))predicate
{
    CGFloat max = FLT_MIN;
    
    for(id item in self)
    {
        CGFloat val = predicate(item);
        if(val > max)
        {
            max = val;
        }
    }
    return max;
}

-(CGFloat)min:(CGFloat(^)(id item))predicate
{
    CGFloat min = FLT_MAX;
    
    for(id item in self)
    {
        CGFloat val = predicate(item);
        if(val < min)
        {
            min = val;
        }
    }
    return min;
}

-(CGFloat)avg:(CGFloat(^)(id item))predicate
{
    CGFloat total = 0;
    CGFloat count = 0;
    
    for(id item in self)
    {
        total += predicate(item);
        count++;
    }
    
    if(count == 0)
    {
        @throw [[NSException alloc] initWithName:NSInvalidArgumentException reason:@"cannot determine average of an empty collection" userInfo:nil];
    }
    return total / count;
}

-(NSString *)componentsJoinedByString:(NSString *)itemSeparator columns:(NSUInteger)columns lineSeparator:(NSString *)lineSeparator
{
    NSMutableString *result = [[NSMutableString alloc] init];
    int col = 0;
    for(NSString *item in self)
    {
        col++;
        [result appendString:item];
        
        if(col >= columns)
        {
            col = 0;
            [result appendString:lineSeparator];
        }
        else
        {
            [result appendString:itemSeparator];
        }
    }
    return result;
}
@end
