//
//  RVCalendarCell.m
//  CalendarDemo
//
//  Created by Rob van der Veer on 14/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "GSCalendarCell.h"

@implementation GSCalendarCell


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setAutoresizingMask:UIViewAutoresizingNone];
    }
    return self;
}

-(void)setDate:(NSDate *)date
{
    _date = date;
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSDayCalendarUnit fromDate:date];
    [self setTitle:[NSString stringWithFormat:@"%ld", (long)components.day] forState:UIControlStateNormal];
}

@end
