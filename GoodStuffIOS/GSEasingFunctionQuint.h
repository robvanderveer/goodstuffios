//
//  GSEasingFunctionQuint.h
//  GoodStuffIOS
//
//  Created by Rob van der Veer on 14/7/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "GSEasingFunction.h"

/**
 Represents an easing function with a quintic exponent. Formula is f(t) = t^5.
 
 ![easeIn](../docs/Images/curveEasingFunctionQuint_easeIn.png =200x150)
 ![easeOut](../docs/Images/curveEasingFunctionQuint_easeOut.png =200x150)
 ![easeInOut](../docs/Images/curveEasingFunctionQuint_easeInOut.png =200x150)
 
 see GSEasingFunction
 
 */
@interface GSEasingFunctionQuint : GSEasingFunction

@end
