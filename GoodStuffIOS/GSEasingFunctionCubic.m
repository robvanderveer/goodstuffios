//
//  GSEasingFunctionCubic.m
//  GoodStuffIOS
//
//  Created by Rob van der Veer on 14/7/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "GSEasingFunctionCubic.h"

@implementation GSEasingFunctionCubic
-(CGFloat)easeCore:(CGFloat)t
{
    return t * t *t;
}
@end
