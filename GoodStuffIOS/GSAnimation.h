//
//  GSAnimation.h
//  GoodStuffIOS
//
//  Created by Rob van der Veer on 14/7/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import "GSEasingFunction.h"

/** Custom keyframe animation class with support for user-defined easing functions. 
 
 The following example with use an Bounce animation to move a layer vertically from its current position to 300.:
 
     GSAnimation *me = [[GSAnimation alloc] initWithPath:@"position.y"];
     me.fillMode = kCAFillModeForwards;
     me.removedOnCompletion = NO;
     me.function = [[GSEasingFunctionBounce alloc] init];
     me.function.mode = EaseOut;
     me.duration = 1;
     me.fromValue = [NSNumber numberWithFloat:_animateView.layer.position.y];
     me.toValue = @300.0;
 
 ![easeOut](../docs/Images/curveEasingFunctionBounce_easeOut.png =200x150)
 
 GoodStuffIOS has several built-in easing functions:
 
  - GSEasingFunctionBack
  - GSEasingFunctionLinear
  - GSEasingFunctionBounce
  - GSEasingFunctionQuint
  - GSEasingFunctionQuart
  - GSEasingFunctionElastic
 
 If you need other functions, you can subclass the GSEasingFunction and override easeCore.
 
 */
@interface GSAnimation : CAKeyframeAnimation
/** Specifies which easing function to use.
 GoodStuffIOS comes with several easing functions like Bounce and Elastic. Check the subclasses of GSEasingFunction for details
 */
@property (nonatomic, strong) GSEasingFunction *function;

/** the propery value to begin the animation. Currently, only NSNumber values are supported */
@property (nonatomic, strong) id fromValue;
/** the propery value for the endpoint of the animation. Currently, only NSNumber values are supported */
@property (nonatomic, strong) id toValue;
/**
 Defines the resolution of the animation, in number of keyframe steps. The default is 128.
 */
@property (nonatomic) NSInteger numberOfSteps;

/** The calculated keyframe values for the given easing function. Internal use only.*/
@property (copy) NSArray *values;

/** creates a new animation for a given keypath
 
 example:
 
    GSAnimation *anim = [GSAnimation alloc] initWithPath:@"location.x"];
 
 @param keyPath the path to add the animation to
 */
-(id)initWithPath:(NSString *)keyPath;
@end

