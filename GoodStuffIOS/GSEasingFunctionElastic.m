//
//  GSEasingFunctionElastic.m
//  GoodStuffIOS
//
//  Created by Rob van der Veer on 14/7/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "GSEasingFunctionElastic.h"

@implementation GSEasingFunctionElastic
-(id)init
{
    self = [super init];
    if(self)
    {
        _springiness = 5;
        _oscillations = 3;
    }
    return self;
}

-(id)initWithSpringiness:(CGFloat)springiness andOscillations:(int)oscillations
{
    self = [super init];
    if(self)
    {
        self.springiness = springiness;
        self.oscillations = oscillations;
    }
    return self;
}

-(CGFloat)easeCore:(CGFloat)t
{
    CGFloat oscillations = MAX(0, self.oscillations);
    CGFloat springiness = MAX(0, self.springiness);
    CGFloat expo;
    
    if(springiness == 0.0)
    {
        expo = t;
    }
    else
    {
        expo = (expf(springiness * t) - 1.0) / (expf(springiness) - 1.0);
    }
    
    return expo * sin((M_PI * 2.0 * oscillations + M_PI * 0.5) * t);
}
@end
