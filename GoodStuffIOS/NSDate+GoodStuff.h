//
//  NSDate+GoodStuff.h
//  CalendarDemo
//
//  Created by Rob van der Veer on 15/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Various utility functions for working with dates
 */
@interface NSDate (GoodStuff)

/**
 Determines if a date is between two other dates (inclusive). The method assumes that the start date is before the end date.
 
 @param start start date
 @param end end date
 @returns true if the date is between the given two other dates.
 */
-(bool)isBetween:(NSDate *)start and:(NSDate *)end;
-(NSInteger)daysApartFrom:(NSDate *)other;
@end
