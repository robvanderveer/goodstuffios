//
//  NSCalendar+GoodStuff.m
//  GoodStuffIOS
//
//  Created by Rob van der Veer on 23/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "NSCalendar+GoodStuff.h"

@implementation NSCalendar (GoodStuff)

-(NSDate *)firstDayOfMonthFor:(NSDate *)date
{
    NSDateComponents *components = [self components:NSTimeZoneCalendarUnit|NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSWeekdayCalendarUnit fromDate:date];
    [components setDay:1];
    [components setHour:0];
    [components setMinute:0];
    [components setSecond:0];
    NSDate *firstOfMonth = [self dateFromComponents:components];
    return firstOfMonth;
}


-(NSDate *)substract:(NSInteger)number units:(NSUInteger)units toDate:(NSDate *)date
{
    return [self add:-number units:units toDate:date];
}

-(NSDate *)add:(NSInteger)number units:(NSUInteger)units toDate:(NSDate *)date
{
    NSDateComponents *components = [[NSDateComponents alloc] init];
    
    switch(units)
    {
        case NSDayCalendarUnit:
            components.day = number;
            break;
        case NSMonthCalendarUnit:
            components.month = number;
            break;
        case NSYearCalendarUnit:
            components.year = number;
            break;
        case NSWeekCalendarUnit:
            components.week = number;
            break;
      /*  case NSWeekdayCalendarUnit:
            components.weekday = number;
            break; */
        default:
            [NSException raise:@"Unsupported component" format:@"%ld", (long)units];
            break;
    }
    NSDate *newDate = [self dateByAddingComponents:components toDate:date options:0];
    return newDate;
}

-(NSDate *)createDate:(NSInteger)day month:(NSInteger)month year:(NSInteger)year
{
    NSDateComponents *components = [[NSDateComponents alloc] init];
    
    components.day = day;
    components.month = month;
    components.year = year;
    
    NSDate *newDate = [self dateFromComponents:components];
    return newDate;
}

-(NSDate *)setPrecision:(NSDate *)date toUnits:(NSUInteger)units
{
    NSUInteger unitFlags;
    
    switch(units)
    {
        case NSDayCalendarUnit:
            unitFlags = NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit;
            break;
        case NSMonthCalendarUnit:
            unitFlags = NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit;
            break;
        case NSYearCalendarUnit:
            unitFlags = NSEraCalendarUnit|NSYearCalendarUnit;
            break;
        default:
            [NSException raise:@"Unsupported component" format:@"%ld", (long)units];
            break;
    }
    
    NSDateComponents *components = [self components:unitFlags fromDate:date];

    NSLog(@"%@", components.timeZone);
    components.hour = 0;
    components.minute = 0;
    components.second = 0;
    components.timeZone = nil;
    
    NSDate *newDate = [self dateFromComponents:components];
    return newDate;
}
@end