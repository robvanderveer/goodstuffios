//
//  FadeBackgroundSegue.h
//  SimplyStats
//
//  Created by Rob van der Veer on 12/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 A custom segue class that fades the background color between UIViewControllers.
 
 It does this by Animating the background property source view (current view) towards
 the background color of the target view
 
 */
@interface GSFadeBackgroundSegue : UIStoryboardSegue
@end
