//
//  GSEasingFunctionCubic.h
//  GoodStuffIOS
//
//  Created by Rob van der Veer on 14/7/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "GSEasingFunction.h"

/**
 Represents an easing function with a cubic exponent.
 
 ![easeIn](../docs/Images/curveEasingFunctionCubic_easeIn.png =200x150)
 ![easeOut](../docs/Images/curveEasingFunctionCubic_easeOut.png =200x150)
 ![easeInOut](../docs/Images/curveEasingFunctionCubic_easeInOut.png =200x150)
 
 see GSEasingFunction
 
 */
@interface GSEasingFunctionCubic : GSEasingFunction

@end
