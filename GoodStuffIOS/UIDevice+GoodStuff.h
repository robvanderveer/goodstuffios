//
//  UIDevice+GoodStuff.h
//  GoodStuffIOS
//
//  Created by Rob van der Veer on 29/7/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <UIKit/UIKit.h>

/** Device Utilities */
@interface UIDevice (GoodStuff)
/** Returns true if the current device has a Retina display */
- (BOOL) hasRetinaDisplay;
@end
