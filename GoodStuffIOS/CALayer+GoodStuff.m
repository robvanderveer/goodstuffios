//
//  CALayer+additions.m
//  SimplyStats
//
//  Created by Rob van der Veer on 7/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "CALayer+GoodStuff.h"

@implementation CALayer (GoodStuff)
- (void)moveToFront {
    CALayer *superlayer = self.superlayer;
    [self removeFromSuperlayer];
    [superlayer addSublayer:self];
}

- (UIImage *)getImageWithSize:(CGSize)size opaque:(bool)opaque contentScale:(CGFloat)scale
{
    CGContextRef context;
    
    size = CGSizeMake(ceilf(size.width), ceilf(size.height));
    scale = roundf(scale);
    
    UIGraphicsBeginImageContextWithOptions(size, opaque, scale);
    context = UIGraphicsGetCurrentContext();
    [self renderInContext:context];
    UIImage *outputImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return outputImg;
}

- (UIImage *)getImageFromLayer
{
    return [self getImageWithSize:self.bounds.size opaque:self.opaque contentScale:self.contentsScale];
}

@end