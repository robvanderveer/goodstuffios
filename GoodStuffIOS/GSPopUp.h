//
//  GSPopUp.h
//  GoodStuffIOS
//
//  Created by Rob van der Veer on 30/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GSPopUp;

typedef bool (^GSPopUpCompletedHandler)(GSPopUp *popup, NSInteger buttonIndex);

/**
 A Custom Popup window base class. 
 
 Note: implementors should call the completed handler to inform the called about a close, let them close the window for you. 
 
 The popup adds a new window to the view hierarchy and listens to device rotation events. Therefore it is important to keep the popup rectangular if possible so it will fit on the screen for both orientations.
 */
@interface GSPopUp : UIView

/** Represents the color used as a backing over the existing window. 
 For the best effect, use a semi-transparent color
 */
@property (nonatomic, strong) UIColor *overlayColor;

/**
 * Shows the popup with a callback handler
 * @param completed A completion block that must be called when the popup is dismissed.
 * @param animated Whether or not to animate the hiding/showing of the popup.
  */
-(void)showWithHandler:(GSPopUpCompletedHandler)completed animated:(BOOL)animated;

/** call this method to hide the popup. A default slide-out animation of 0.3 seconds is used.
 * @param animated Specify if the removal should be animated.
 */
-(void)hide:(BOOL)animated;

/** subclasses of GSPopup should call this method when a button is pressed or when the popup is to be dismissed. 
 @param buttonIndex index of the button that has been selected to dismissed the popup.
 */
-(bool)callHandler:(NSInteger)buttonIndex;
@end
