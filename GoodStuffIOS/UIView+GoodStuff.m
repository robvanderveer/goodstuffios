//
//  UIView+GoodStuff.m
//  GoodStuffIOS
//
//  Created by Rob van der Veer on 3/1/15.
//  Copyright (c) 2015 Rob van der Veer. All rights reserved.
//

#import "UIView+GoodStuff.h"

@implementation UIView (GoodStuff)

// https://stackoverflow.com/questions/4334233/how-to-capture-uiview-to-uiimage-without-loss-of-quality-on-retina-display/25585958#25585958

- (UIImage *)imageByRenderingView
{
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, [[UIScreen mainScreen] scale]);
    [self drawViewHierarchyInRect:self.bounds afterScreenUpdates:YES];
    UIImage * snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return snapshotImage;
}

@end