//
//  GSRenderedView.h
//  GoodStuffIOS
//
//  Created by Rob van der Veer on 14/1/15.
//  Copyright (c) 2015 Rob van der Veer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GSRenderedView : UIImageView

@property (nonatomic) bool renderOpaque;
@property (nonatomic) CGFloat renderScaleFactor;

-(NSBlockOperation *)renderInQueue:(NSOperationQueue *)queue completion:(void (^)(void))completed;
-(void)renderWithOperation:(NSBlockOperation *)operation;
-(UIImage *)renderToImageWithOperation:(NSBlockOperation *)operation;

@end
