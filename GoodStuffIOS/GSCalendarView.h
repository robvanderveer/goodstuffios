//
//  RVCalendarView.h
//  CalendarDemo
//
//  Created by Rob van der Veer on 14/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GSCalendarCell.h"
#import "GSVisualPreferences.h"

@class GSCalendarView;

/**
 Delegate used for receiving GSCalendarView events
 */
@protocol GSCalendarViewDelegate <NSObject>

/**
 * This delegate is called when another date is selected
 * @param sender The GSCalendarView that triggered the event
 * @param date The newly selected date
 */
-(void)didChangeSelection:(GSCalendarView *)sender newDate:(NSDate *)date;

@end

/** A grid-style month calendar view showing all days for a given month 
 *
 * @see GSDatePicker
 */
@interface GSCalendarView : UIView

/** Specifies the display date. This date is used to determine which month should be displayed */
@property (nonatomic, strong) NSDate *displayDate;

/** Specifies the startDate, which can be used to display a range selection 
 *
 * startDate should be <= endDate
 * see @endDate
 */
@property (nonatomic, strong) NSDate *startDate;

/** Specifies the endDate, which can be used to display a range selection.
 *
 * endDate should be >= startDate
 * see @startDate
 */
@property (nonatomic, strong) NSDate *endDate;

/** Specifies the maximum selectable date */
@property (nonatomic, strong) NSDate *maximumDate;

/** Specifies the earliest date that can be selected */
@property (nonatomic, strong) NSDate *minimumDate;

/** Specifies the calendar to be used. Defaults to the current locale calendar. */
@property (nonatomic, strong) NSCalendar *calendar;

/** The delegate for this instance 
 * 
 * @see GSCalendarViewDelegate
 */
@property (nonatomic, strong) id<GSCalendarViewDelegate> delegate;

/** Visual appearance settings
 * 
 * @see GSVisualPreferences
 */
@property (nonatomic, strong) GSVisualPreferences *appearance;
@end
