//
//  GSColumnView.h
//  GoodStuffIOS
//
//  Created by Rob van der Veer on 04-02-15.
//  Copyright (c) 2015 Rob van der Veer. All rights reserved.
//

#import <UIKit/UIKit.h>

// Columns will child views (regular UIViews) into aligned rows
// It inspects all child view for the number of children and their max widths
// and on LayOut it will adjust all the frames accordingly.


@interface GSColumnView : UIView
@property (nonatomic) NSInteger cellSpacing;
@end
