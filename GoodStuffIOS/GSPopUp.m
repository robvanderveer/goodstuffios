//
//  GSPopUp.m
//  GoodStuffIOS
//
//  Created by Rob van der Veer on 30/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "GSPopUp.h"

@interface GSPopUp ()
@property (nonatomic, strong) GSPopUpCompletedHandler handler;
@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) UIWindow *parentWindow;
@end

@implementation GSPopUp
-(void)initCommon
{
    self.overlayColor = [UIColor colorWithWhite:0 alpha:0.5];
    self.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
}

-(id)init
{
    self = [super init];
    if(self)
    {
        [self initCommon];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        [self initCommon];
    }
    return self;
    
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        [self initCommon];
    }
    return self;
}

-(void)showWithHandler:(GSPopUpCompletedHandler)completed animated:(BOOL)animated
{
    CGRect bounds = [[UIScreen mainScreen] bounds];
    
    _parentWindow = [[[UIApplication sharedApplication] windows] firstObject];

    _containerView = [[UIView alloc] initWithFrame:bounds];
    _containerView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    [_containerView addSubview:self];

    if ([UIBlurEffect class])
    {
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        UIVisualEffectView *blurredEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        [blurredEffectView setAlpha:0.7];
        [blurredEffectView setFrame:bounds];
        
        [_containerView insertSubview:blurredEffectView atIndex:0];
    }
    else
    {
        _containerView.backgroundColor = self.overlayColor;
    }
    
    self.handler = completed;
    self.center = _containerView.center;
    [_parentWindow addSubview:_containerView];
    [self.window makeKeyAndVisible];

    //    [self rotateAccordingToStatusBarOrientationAndSupportedOrientations];
    
    if(animated)
    {
        _containerView.alpha = 0;
 
        [UIView animateWithDuration:0.3
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             _containerView.alpha = 1;
                             
                         }
                         completion:nil];
    }
    
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarFrameOrOrientationChanged:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarFrameOrOrientationChanged:) name:UIApplicationDidChangeStatusBarFrameNotification object:nil];
}

-(void)hide:(BOOL)animated
{
    
    if(animated)
    {
        [UIView animateWithDuration:0.3 delay:0 options:0 animations:^{
            _containerView.alpha = 0;
            self.center = CGPointMake(CGRectGetMidX(self.window.bounds),0);
        } completion:^(BOOL finished) {
            [self hide:false];
        }];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        
        [self removeFromSuperview];
        [_containerView removeFromSuperview];
    }
}

-(bool)callHandler:(NSInteger)buttonIndex
{
    if(_handler)
    {
        return _handler(self, buttonIndex);
    }
    
    return true;
}

//- (void)statusBarFrameOrOrientationChanged:(NSNotification *)notification
//{
//    /*
//     This notification is most likely triggered inside an animation block,
//     therefore no animation is needed to perform this nice transition.
//     */
//    [self rotateAccordingToStatusBarOrientationAndSupportedOrientations];
//}
//
//
//- (void)rotateAccordingToStatusBarOrientationAndSupportedOrientations
//{
//    UIInterfaceOrientation statusBarOrientation = [UIApplication sharedApplication].statusBarOrientation;
//    CGFloat angle = UIInterfaceOrientationAngleOfOrientation(statusBarOrientation);
//    //    CGFloat statusBarHeight = [[self class] getStatusBarHeight];
//    
//    CGAffineTransform transform = CGAffineTransformMakeRotation(angle);
//    
//    [self setIfNotEqualTransform:transform];
//}
//
//- (void)setIfNotEqualTransform:(CGAffineTransform)transform
//{
//    if(!CGAffineTransformEqualToTransform(self.transform, transform))
//    {
//        self.transform = transform;
//    }
//}
//
//+ (CGFloat)getStatusBarHeight
//{
//    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
//    if(UIInterfaceOrientationIsLandscape(orientation))
//    {
//        return [UIApplication sharedApplication].statusBarFrame.size.width;
//    }
//    else
//    {
//        return [UIApplication sharedApplication].statusBarFrame.size.height;
//    }
//}
//
//+ (CGRect)rectInWindowBounds:(CGRect)windowBounds statusBarOrientation:(UIInterfaceOrientation)statusBarOrientation statusBarHeight:(CGFloat)statusBarHeight
//{
//    CGRect frame = windowBounds;
//    frame.origin.x += statusBarOrientation == UIInterfaceOrientationLandscapeLeft ? statusBarHeight : 0;
//    frame.origin.y += statusBarOrientation == UIInterfaceOrientationPortrait ? statusBarHeight : 0;
//    frame.size.width -= UIInterfaceOrientationIsLandscape(statusBarOrientation) ? statusBarHeight : 0;
//    frame.size.height -= UIInterfaceOrientationIsPortrait(statusBarOrientation) ? statusBarHeight : 0;
//    return frame;
//}
//
//CGFloat UIInterfaceOrientationAngleOfOrientation(UIInterfaceOrientation orientation)
//{
//    CGFloat angle;
//    
//    switch (orientation)
//    {
//        case UIInterfaceOrientationPortraitUpsideDown:
//            angle = M_PI;
//            break;
//        case UIInterfaceOrientationLandscapeLeft:
//            angle = -M_PI_2;
//            break;
//        case UIInterfaceOrientationLandscapeRight:
//            angle = M_PI_2;
//            break;
//        default:
//            angle = 0.0;
//            break;
//    }
//    
//    return angle;
//}
//
//UIInterfaceOrientationMask UIInterfaceOrientationMaskFromOrientation(UIInterfaceOrientation orientation)
//{
//    return 1 << orientation;
//}

@end
