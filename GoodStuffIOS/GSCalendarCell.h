//
//  RVCalendarCell.h
//  CalendarDemo
//
//  Created by Rob van der Veer on 14/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 Utility class for GSCalendarView. Internal use only.
 */
@interface GSCalendarCell : UIButton

/** Date property for this calendar cell */
@property (nonatomic, strong) NSDate *date;
@end
