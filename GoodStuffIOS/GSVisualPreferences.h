//
//  RVVisualPreferences.h
//  CalendarDemo
//
//  Created by Rob van der Veer on 14/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/** Reusable block of visual settings 
   Instead of overloading each custom `UIView` with properties to specify different settings, use `GSVisualPreferences` to
collect all these settings once in your `AppDelegate`. Use the <+standardAppearance> singleton to share settings for the entire app.
 */

@interface GSVisualPreferences : NSObject

/** default font setting */
@property (nonatomic, strong) UIFont *font;

/** font to be used for a smaller font */
@property (nonatomic, strong) UIFont *smallFont;

/** font to be used as a large header font */
@property (nonatomic, strong) UIFont *largeFont;

/** the default lineWidth to use when drawing */
@property (nonatomic) CGFloat lineWidth;

/** The default fill color */
@property (nonatomic, strong) UIColor *fillColor;

/** The default border color */
@property (nonatomic, strong) UIColor *borderColor;

/** The default text color */
@property (nonatomic, strong) UIColor *textColor;

/** The default text color for a disabled element */
@property (nonatomic, strong) UIColor *disabledTextColor;

/** The default fill color for an active element */
@property (nonatomic, strong) UIColor *activeFillColor;

/** The background color for a normal item */
@property (nonatomic, strong) UIColor *backgroundColor;

/** `standardAppearance` returns a singleton reference to a `GSVisualPreferences` object.
 *
 * Use this method to access an preferences set that you can use throughout your application.
 */
+(GSVisualPreferences *)standardAppearance;

/**
 * intializes a new VisualPreferences object by cloning the given preferences set.
 * @param prefs The preferences set to clone
 * @return The initializes preferences set.
 * @see copy
 */
-(GSVisualPreferences *)initWith:(GSVisualPreferences *)prefs;

/**
 * Copies a visual preferences object. Similar to initWith
 * @see initWith:
 */
-(GSVisualPreferences *)copy;
@end
