//
//  GSEasingFunctionBack.h
//  GoodStuffIOS
//
//  Created by Rob van der Veer on 14/7/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "GSEasingFunction.h"

/**
 Represents a function for an animation that pulls back a little before setting off.
 
 ![easeIn](../docs/Images/curveEasingFunctionBack_easeIn.png =200x150)
 ![easeOut](../docs/Images/curveEasingFunctionBack_easeOut.png =200x150)
 ![easeInOut](../docs/Images/curveEasingFunctionBack_easeInOut.png =200x150)
 
  see GSEasingFunction
 
 */
@interface GSEasingFunctionBack : GSEasingFunction

/** Sets or gets the amplitude of the bounce. Default value is 0.3, which means that the pullback will be 30% of the entire animation distance */
@property (nonatomic) CGFloat amplitude;

/**
 * initializes the instance with a specified amplitude 
 * 
 * @param amplitude See amplitude
 */
-(id)initWithAmplitude:(CGFloat)amplitude;
@end
