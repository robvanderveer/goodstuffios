//
//  RVGridView.m
//  CalendarDemo
//
//  Created by Rob van der Veer on 14/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "GSGridView.h"

@implementation GSGridView

-(void)initCommon
{
    _columns = 5;
    _animateLayout = true;
    _animationDuration = 0.5;
    [self setAutoresizesSubviews:NO];
    [self setAutoresizingMask:UIViewAutoresizingNone];
}

-(id)init
{
    self = [super init];
    if(self)
    {
        [self initCommon];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initCommon];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initCommon];
    }
    return self;    
}

-(void)setColumns:(NSInteger)columns
{
    if(_columns != columns)
    {
        _columns = columns;
        [self setNeedsLayout];
    }
}

-(void)setPadding:(CGFloat)padding
{
    if(_padding != padding)
    {
        _padding = padding;
        [self setNeedsLayout];
    }
}

-(void)setSpacing:(CGFloat)spacing
{
    if(_spacing != spacing)
    {
        _spacing = spacing;
        [self setNeedsLayout];
    }
}

-(void)updateLayout
{
    if(self.columns == 0 || self.subviews.count == 0)
    {
        return;
    }
    
    //re-layout the child dates.
    CGFloat w = self.bounds.size.width - (_padding * 2);
    CGFloat h = self.bounds.size.height - (_padding * 2);
    int rows = ceilf((float)self.subviews.count / self.columns);
    
    //for every column-1, substract 'spacing'.
    CGFloat horizontalSpacing = (self.columns-1) * _spacing;
    CGFloat verticalSpacing = (rows - 1) * _spacing;
    
    CGSize size = CGSizeMake((w - horizontalSpacing) / self.columns,
                             (h - verticalSpacing) / rows);
    
    if(_animateLayout)
    {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:_animationDuration];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    }
    //grid layout all child views.
    int i = 0;
    for(UIView *childView in self.subviews)
    {
        CGRect childFrame = CGRectMake(((i % self.columns) * (size.width + _spacing)) + _padding,
                                       ((i / self.columns) * (size.height + _spacing)) + _padding,
                                       size.width,
                                       size.height);
        
        childView.frame = childFrame;
        i++;
    }
    
    if(_animateLayout)
    {
        [UIView commitAnimations];
    }
}

-(void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    [self updateLayout];
}

-(void)didAddSubview:(UIView *)subview
{
    [self setNeedsLayout];
}
@end
