//
//  RVCalendarView.m
//  CalendarDemo
//
//  Created by Rob van der Veer on 14/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "GSCalendarView.h"
#import "GSGridView.h"
#import "UIColor+GoodStuff.h"
#import "NSDate+GoodStuff.h"
#import "NSCalendar+GoodStuff.h"

@interface GSCalendarView ()
//private properties
@property (nonatomic, strong) GSGridView *dayGrid;
@property (nonatomic, strong) GSGridView *dayNames;

@end

@implementation GSCalendarView

-(void)initCommon
{
    _calendar = [NSCalendar currentCalendar];
    _displayDate = [_calendar setPrecision:[NSDate date] toUnits:NSDayCalendarUnit];
    _appearance = [GSVisualPreferences standardAppearance];
    _maximumDate = [NSDate date];   //cannot select tomorrow for instance;
    _minimumDate = [_calendar substract:10 units:NSYearCalendarUnit toDate:[NSDate date]];
    
    [self setAutoresizesSubviews:NO];
    [self setAutoresizingMask:UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initCommon];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        [self initCommon];
    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    //re-layout the child dates.
    CGFloat w = self.bounds.size.width;
    CGFloat h = self.bounds.size.height;
    
    if(!_dayNames)
    {
        _dayNames = [[GSGridView alloc] init];
        [self fillDayNames];
        [self addSubview:_dayNames];
        
        [self fillDayNames];
    }
    
    if(!_dayGrid)
    {
        _dayGrid = [[GSGridView alloc] init];
        _dayGrid.columns = 7;
        _dayGrid.animateLayout = false;
        [self addSubview:_dayGrid];
        
        [self updateCalendar];
    }
    
    self.dayNames.frame = CGRectMake(0, 0, w, 30);
    self.dayGrid.frame = CGRectMake(0, 30, w, h-30);
    [self.dayNames setNeedsLayout];
    [self.dayGrid setNeedsLayout];
}

-(void)setAppearance:(GSVisualPreferences *)appearance
{
    _appearance = appearance;
    
    //recreate daynames.
    [_dayNames removeFromSuperview];
    _dayNames = nil;
    
    //rebuild calendar.
    [_dayGrid removeFromSuperview];
    _dayGrid = nil;
    
    [self layoutSubviews];
}

-(NSDate *)firstDateForDisplay
{
    //transform the displayDate to the first 'firstWeekDay' of the month.
    //e.g. march 15 could be 27 february.
    NSDateComponents *components = [[self calendar] components:NSTimeZoneCalendarUnit|NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSWeekdayCalendarUnit fromDate:[self displayDate]];
    [components setDay:1];
    [components setHour:0];
    [components setMinute:0];
    [components setSecond:0];
    NSDate *firstOfMonth = [[self calendar] dateFromComponents:components];
    
    NSUInteger firstWeekDay = [[self calendar] firstWeekday];

    NSDateComponents *adjustComponents = [[NSDateComponents alloc] init];
    [adjustComponents setDay:-(components.weekday - (firstWeekDay -1))];
    
    NSDate *date = [[self calendar] dateByAddingComponents:adjustComponents toDate:firstOfMonth options:0];
    
    return date;
}

-(void)fillDayNames
{
    [[_dayNames subviews] makeObjectsPerformSelector: @selector(removeFromSuperview)];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setCalendar:self.calendar];
    NSUInteger firstWeekday = [self.calendar firstWeekday];
    
    NSArray *weekdayNames = [formatter shortWeekdaySymbols];
        
    _dayNames.columns = weekdayNames.count;
    for(int day = 0; day < weekdayNames.count; day++)
    {
        NSString *weekdayName = weekdayNames[(day + (firstWeekday-1))%7];
        
        UILabel *dayLabel = [[UILabel alloc] init];
        dayLabel.text = weekdayName;
        dayLabel.textColor = self.appearance.textColor;
        dayLabel.backgroundColor = self.appearance.fillColor;
        dayLabel.textAlignment = NSTextAlignmentCenter;
        dayLabel.font = self.appearance.smallFont;
        [_dayNames addSubview:dayLabel];
    }
}

-(void)updateCalendar
{
    //    NSDate *methodStart = [NSDate date];
    NSDate *dateToDisplay = [self firstDateForDisplay];
    
    NSDateComponents *compDisplay = [[self calendar] components:NSMonthCalendarUnit fromDate:[self displayDate]];
    NSDateComponents *comp = [[self calendar] components:NSMonthCalendarUnit fromDate:dateToDisplay];
        
    NSDateComponents *addOne = [[NSDateComponents alloc] init];
    [addOne setDay:1];
    
    UIImage *imgFillColor = [self.appearance.fillColor getSolidImage];
    UIImage *imgActiveFillColor = [self.appearance.activeFillColor getSolidImage];
    UIImage *imgTextColor = [self.appearance.activeFillColor getSolidImage];
    
    NSLog(@"%@ %@ %@", _minimumDate, dateToDisplay, _maximumDate);
    
    int index = 0;
    for(int weekNumber = 0; weekNumber < 6; weekNumber++)
    {
        //render a week.
        for(int i = 0; i < 7; i++)
        {
            GSCalendarCell *dateCell;
            if(index < _dayGrid.subviews.count)
            {
                dateCell = _dayGrid.subviews[index];
            }
            else
            {
                //create a cell.
                dateCell = [[GSCalendarCell alloc] initWithFrame:CGRectZero];
                //normal
                [dateCell setBackgroundImage:imgFillColor forState:UIControlStateNormal];
                [dateCell setTitleColor:self.appearance.textColor forState:UIControlStateNormal];
                
                //disabled
                [dateCell setTitleColor:self.appearance.disabledTextColor forState:UIControlStateDisabled];
                
                //selected + disabled
                [dateCell setTitleColor:self.appearance.disabledTextColor forState:UIControlStateDisabled|UIControlStateSelected];
                [dateCell setBackgroundImage:imgActiveFillColor forState:UIControlStateDisabled|UIControlStateSelected];
                
                //selected
                [dateCell setTitleColor:self.appearance.textColor forState:UIControlStateSelected];
                [dateCell setBackgroundImage:imgActiveFillColor forState:UIControlStateSelected];
                
                //hilighted.
                [dateCell setTitleColor:self.appearance.borderColor forState:UIControlStateHighlighted];
                [dateCell setBackgroundImage:imgTextColor forState:UIControlStateHighlighted];
                
                //selected + hilighted
                [dateCell setTitleColor:self.appearance.borderColor forState:UIControlStateSelected|UIControlStateHighlighted];
                [dateCell setBackgroundImage:imgTextColor forState:UIControlStateSelected|UIControlStateHighlighted];
                
                [dateCell addTarget:self action:@selector(dateTouched:) forControlEvents:UIControlEventTouchUpInside];
                [dateCell setOpaque:YES];
                
                [[dateCell titleLabel] setAdjustsFontSizeToFitWidth:false];
                [[dateCell titleLabel] setFont:self.appearance.font];
                
                [_dayGrid addSubview:dateCell];
            }
            
            [dateCell setDate:dateToDisplay];
            
            bool isInRange = [dateToDisplay isBetween:_minimumDate and:_maximumDate];
            bool isSelected = [dateToDisplay isBetween:_startDate and:_endDate];
            
            if(!isInRange)
            {
                //additional alpha
                [dateCell setAlpha:0.3];
            }
            else
            {
                [dateCell setAlpha:1];
            }
            
            [dateCell setEnabled:isInRange && (comp.month == compDisplay.month)];
            [dateCell setSelected:isSelected];
            
            index++;
            dateToDisplay = [[self calendar] dateByAddingComponents:addOne toDate:dateToDisplay options:0];
            
            comp = [[self calendar] components:NSMonthCalendarUnit fromDate:dateToDisplay];
        }
    }
    
//    NSDate *methodFinish = [NSDate date];
//    NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:methodStart];
//    
//    NSLog(@"buildday: %lf", executionTime);
}

-(void)dateTouched:(id)sender
{
    GSCalendarCell *dateCell = sender;
    NSLog(@"date: %@", dateCell.date);
    
    [self.delegate didChangeSelection:self newDate:dateCell.date];
    
//    [self setStartDate:dateCell.date];
//    [self setEndDate:dateCell.date];
}

#pragma mark -
#pragma mark properties

-(void)setDisplayDate:(NSDate *)displayDate
{
    _displayDate = [_calendar setPrecision:displayDate toUnits:NSDayCalendarUnit];

    [self updateCalendar];
}

-(void)setStartDate:(NSDate *)startDate
{
    startDate = [_calendar setPrecision:startDate toUnits:NSDayCalendarUnit];

    if(startDate != _startDate)
    {
        _startDate = startDate;
        [self updateCalendar];
    }
}

-(void)setEndDate:(NSDate *)endDate
{
    endDate = [_calendar setPrecision:endDate toUnits:NSDayCalendarUnit];
    
    if(_endDate != endDate)
    {
        _endDate = endDate;
        [self updateCalendar];
    }
}

@end
