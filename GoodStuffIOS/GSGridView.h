//
//  RVGridView.h
//  CalendarDemo
//
//  Created by Rob van der Veer on 14/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 Self-layouting simple grid view. 
 
 The GSGridView uses a `padding` and `spacing` to layout the childviews in rectangular tiles, using the `columns` property. There is no need to call `setNeedsLayout` after changing any of the properties. 
 */
@interface GSGridView : UIView

/** Represents the number of columns that the gridview should use when layout is due.
 
 Default value: 5 columns.
 */
@property (nonatomic) NSInteger columns;

/** Represents the padding on the outside of the tiles. Defaults to 0. */
@property (nonatomic) CGFloat padding;

/** Represents the spacing between the tiles. Defaults to 0. */
@property (nonatomic) CGFloat spacing;

/** Represents a setting whether or not to animate child view position and size. Default value is `true` */
@property (nonatomic) bool animateLayout;


/** Represents the duration for the automatic layout animation. Defaults to 0.5 */
@property (nonatomic) NSTimeInterval animationDuration;
@end
