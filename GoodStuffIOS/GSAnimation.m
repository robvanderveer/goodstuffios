//
//  GSAnimation.m
//  GoodStuffIOS
//
//  Created by Rob van der Veer on 14/7/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "GSAnimation.h"

@implementation GSAnimation
-(void)initCommon
{
    _numberOfSteps = 128;
    self.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
}

-(id)initWithPath:(NSString *)keyPath
{
    self = [super init];
    if(self)
    {
        self.keyPath = keyPath;
        [self initCommon];
    }
    return self;
}

-(void)setToValue:(id)toValue
{
    _toValue = toValue;
    [self buildValues];
}

-(void)setFromValue:(id)fromValue
{
    _fromValue = fromValue;
    [self buildValues];
}

-(void)setFunction:(GSEasingFunction *)function
{
    _function = function;
    [self buildValues];
}

-(void)setNumberOfSteps:(NSInteger)numberOfSteps
{
    _numberOfSteps = numberOfSteps;
    [self buildValues];
}

-(void)buildValues
{
    //no need to build values when we are missing some data.
    if(!self.fromValue || !self.toValue)
    {
        return;
    }
    
    if([self.fromValue isKindOfClass:[NSNumber class]])
    {
        [self setValues:[self getValueArray:[_fromValue floatValue] endValue:[_toValue floatValue]]];
    }
    else if([self.fromValue isKindOfClass:[NSValue class]])
    {
        // support other structure types.
    }
};

-(NSArray *)getValueArray:(CGFloat)startValue endValue:(CGFloat)endValue
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    CGFloat base = startValue;
    CGFloat diff = endValue - startValue;
    
    for(int i = 0; i < _numberOfSteps; i++)
    {
        CGFloat t = i/(CGFloat)_numberOfSteps;
        CGFloat value = [[self function] ease:t];
        CGFloat normalizedValue = base + (value * diff);
        
        [result addObject:[NSNumber numberWithFloat:normalizedValue]];
    }
    return result;
}
@end
