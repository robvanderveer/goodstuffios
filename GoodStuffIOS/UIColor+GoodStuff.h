//
//  UIColor+additions.h
//  SimplyStats
//
//  Created by Rob van der Veer on 11/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 Several categorie method for working with UIColor.
 */
@interface UIColor (GoodStuff)

/** Blends the color with another color using a percentage.
 * The effect is that of overlapping the new color with a given alpha.
 *
 * @param other The color to mix with
 * @param percentage The transparency of the mixing color.
 */
- (UIColor *)blendWith:(UIColor *)other mix:(CGFloat)percentage;

/**
 * Generates a contrasting color
 */
- (UIColor *)getContrastColor;

/**
 * Generates the inverse color
 */
- (UIColor *)getInverseColor;

/** Returns a slightly brighter variant.
 *
 * The Hue component is multiplied by 2
 */
- (UIColor *)getBrighter;

/** Returns a slightly darker variant 
 *
 * The Hue component is divided by 2
 */
- (UIColor *)getDarker;

/** Returns a solid color image, size 16x16 */
- (UIImage *)getSolidImage;

/** Constructs a UIColor from a string */
+(UIColor *)colorFromHexString:(NSString *)hex;
@end
