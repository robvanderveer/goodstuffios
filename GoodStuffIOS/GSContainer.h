//
//  GSContainer.h
//  GoodStuffIOS
//
//  Created by Rob van der Veer on 28/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GSContainer;

typedef id (^GSConstructionBlock)(GSContainer *container);

/**
 A Basic IOC Container
 */
@interface GSContainer : NSObject

/**
 * Adds an object type to the container
 * @param protocol The protocol to register
 * @param constructor The constructor used to instantiate an object matching the protocol
 */
-(void)setup:(Protocol *)protocol with:(GSConstructionBlock)constructor;

/**
 * Adds an object type to the container
 * @param protocol The protocol to register
 * @param name The identifier that is used to register the protocol instance.
 * @param constructor The constructor used to instantiate an object matching the protocol
 */
-(void)setup:(Protocol *)protocol named:(NSString *)name with:(GSConstructionBlock)constructor;

/**
 * Resolves a protocol to retrieve an object that implements the protocol
 * @param protocol The protocol for which to retrieve an instance
 * @return An object that implements the protocol, if it was registered with the container. Otherwise, nil.
 */
-(id)obtain:(Protocol *)protocol;

/**
 * Resolves a protocol for a name to retrieve an object that implements the protocol
 *
 * Use the named resolve method when the default registered protocol is not sufficient enough.
 * Remember that a named protocol must have been set up.
 *
 * @param protocol The protocol for which to retrieve an instance
 * @param name The name that was used to register the protocol instance.
 * @return An object that implements the protocol, if it was registered with the container. Otherwise, nil.
 */
-(id)obtain:(Protocol *)protocol named:(NSString *)name;
@end
