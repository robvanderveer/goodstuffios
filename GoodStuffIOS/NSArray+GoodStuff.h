//
//  NSArray+GoodStuff.h
//  GoodStuffIOS
//
//  Created by Rob van der Veer on 5/7/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef bool (^GSPredicate)(id element);

/** Extension methods for NSArray. For those with experience with C# and LINQ, these methods will be very familiar.
 Unfortunately Objective C does not support the lambda style notation, but with block functions it will be a little easier. I've tried
 to copy the naming convention and functionality of the Microsoft CLR.
 
 These LINQ style extension classes will help you to use fewer awkward for loops for common
 operations like selection and filtering. Consider the following example where both fragments perform the same function:
 
    //example 1
    CALayer *foundLayer;
    for(CALayer *layer in self.view.sublayers)
    {
        if(layer.opacity == 0)
        {
            foundLayer = layer;
            break;
        }
    }
 
    //example 2
    CALayer *foundLayer = [self.view.sublayers firstOrDefault:^(CALayer *layer) { return layer.opacity == 0; }];
 
 Another advantage is that these extensions are chainable like so:
 
    User *administrator = [[members ofClass:[Administrator Class]] singleOrDefault];
 
 Which will even throw an exception if there is more than one administrator.
 */
@interface NSArray (GoodStuff)

/** @name Projecting items */

/** Projects a collection to a new collection by using the block parameter to convert one item into a new item.

 The following example shows how an array of strings is transformed to a new array that contains the lengths of each string:
 
    NSArray *elements = @{@"one", @"two", @"three" };
    NSArray *lengths = [elements select:^(NSString *item) { return item.length; }];
 
 @param function Block argument to transform an element into a response element.
 @returns An NSArray containing the transformed items.
 */
-(NSArray *)select:(id (^)(id item))function;

/** @name Filtering items */

/** Returns only the items of the array that are from the given class (or inherits from the given class).

 Example:
 
    NSArray *admins = [users ofClass:[Administrator Class]];
 
 @param class the class to filter the elements on
 @returns An NSArray that only contains the elements that match the class
 */
-(NSArray *)ofClass:(Class)class;

/** Returns only the items that match the predicate.
 
 The following example demonstrates the usage of this method. The variable named 'users' contains an array of Accounts with different
 accounts. This example collects only the users that are administrators.
 
    NSArray *admins = [users where:^(Account *user) { return user.IsAdministrator; }];
 
 @param predicate Predicate used to determine if the item should be returned.
 @returns An NSArray that only contains the elements that match the predicate.
 */
-(NSArray *)where:(GSPredicate)predicate;

/** Skip a number of elements
 
 Skip returns a collection that does not contain the first 'n' elements, effectively skipping items. 
 If the collection contains fewer elements as the parameter, the method returns an empty collection.
 
 For example:
 
    NSArray *tokens = @[@"one", @"two", @"three"];
    NSArray *items = [tokens skip:1];
 
 The result is that 'items' with only contains the strings "two", and "three". 
 
 @param number The number of elements to skip.
 @returns An NSArray
 */
-(NSArray *)skip:(NSInteger)number;

/** Take a selection
 
 Take returns a collection that contains no more than first 'n' elements
 If the collection contains fewer elements as the parameter, the method returns the unchanged collection.
 
 For example:
 
    NSArray *tokens = @[@"one", @"two", @"three"];
    NSArray *items = [tokens take:2];
 
 The result is that 'items' with only contains the strings "one", and "two".
 
 @param number The number of elements to take.
 @returns An NSArray
 */
-(NSArray *)take:(NSInteger)number;

/** Returns the first item in the collection, if any */
-(id)firstOrDefault;

/** Returns the first item in the collection that matches a predicate, if any 
 
 @param predicate A function to test each element of the collection
*/
-(id)firstOrDefault:(bool (^)(id item))predicate;

/** Returns the last item in the collection, if any */
-(id)lastOrDefault;

/** Returns the last item in the collection that matches the predicate, if any
 
 @param predicate A function to test each element of the collection
*/
-(id)lastOrDefault:(bool (^)(id item))predicate;

/** Returns the single item in the collection.
 
 If there is more than one item in the collection, this method will throw an NSInvalidArgumentException. The primary
 use of this method is to assert that an item is unique. It is mostly used in conjunction with the where method.
 */
-(id)singleOrDefault;

/** @name Logic functions */

/** Returns true if at least one the elements matches the predicate.
 
 Note that if the collection is empty, this method returns false.
 @param predicate A function to test each element of the collection
 @returns true if all the elements in the collection match the predicate, false otherwise.
 */
-(bool)any:(bool (^)(id item))predicate;

/** returns true if all the elements match the predicate
 
 Note that if the collection is empty, this method returns true.
 
 @param predicate A function to test each element of the collection
 @returns true if every element of the source sequence passes the test in the specified predicate, or if the sequence is empty; otherwise, false.
 */
-(bool)all:(bool (^)(id item))predicate;

/** @name Generating items */

/**
 * Generates a range of elements by enumerating an integer from start to end
 *
 * This method generates an array with a generated element for a series of numbers. A typical usage would be to generate values for a function.
 *
 * Example:
 *
 *     NSArray *quads = [NSArray range:-10 to:10 with:^(NSInteger i) { return [NSValue valueFromFloat:i^4; }];
 *
 * @param from The number to begin with
 * @param to The number to end with
 * @param function The function block to evaluate for each number
 * @return An NSArray of generated images
 */
+(NSArray *)range:(NSInteger)from to:(NSInteger)to with:(id (^)(NSInteger value))function;

/** @name Performing calculations */

/** Calculates the sum of all the items in the array

Example:
 
    @interface MyObject : NSObject
    @property (nonatomic) CGFloat numberValue;
    @end
 
    @class MyObject
    @end
 
    ...
 
    NSArray *objects = ... // array initialized with MyObject objects.
    CGFloat total = [objects sum:^(MyObject *item) { 
                        return item.numberValue;
                     }];
    NSLog(@"Total value: %f", total);
 
 @param predicate the predicate to get a value from an element
 */
-(CGFloat)sum:(CGFloat(^)(id item))predicate;

/**
 * Calculates the maximum of all the items in the array
 * @param predicate the predicate to get a value from an element
 * @see sum:
 */
-(CGFloat)max:(CGFloat(^)(id item))predicate;

/**
 * Calculates the minimum of all the items in the array
 * @param predicate the predicate to get a value from an element
 * @see sum:
 */
-(CGFloat)min:(CGFloat(^)(id item))predicate;

/**
 * Calculates the average of all the items in the array
 *
 * @warning the method will throw an exception if the array contains no elements.
 * @param predicate the predicate to get a value from an element
 * @see sum:
 */
-(CGFloat)avg:(CGFloat(^)(id item))predicate;

/**
 * Joins all NSString components by using a columnar layout and a custom line separator
 * @param itemSeparator The separator between two columns
 * @param columns The number of columns
 * @param lineSeparator The separator for a new line
 */
-(NSString *)componentsJoinedByString:(NSString *)itemSeparator columns:(NSUInteger)columns lineSeparator:(NSString *)lineSeparator;
@end
