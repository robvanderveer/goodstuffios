//
//  GSEasingFunctionBounce.m
//  GoodStuffIOS
//
//  Created by Rob van der Veer on 14/7/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "GSEasingFunctionBounce.h"

@implementation GSEasingFunctionBounce

-(id)init
{
    self = [super init];
    if(self)
    {
        self.bounces = 3;
        self.bounciness = 2;
    }
    return self;
}

-(id)initWithBounces:(int)bounces
{
    return [self initWithBounces:bounces andBounciness:2];
}

-(id)initWithBounces:(int)bounces andBounciness:(CGFloat)bounciness
{
    self = [super init];
    if(self)
    {
        self.bounces = bounces;
        self.bounciness = bounciness;
    }
    return self;
}

float logx(float value, float base)
{
    return log10f(value) / log10f(base);
}

-(CGFloat)easeCore:(CGFloat)t
{
    CGFloat bounces = MAX(0, [self bounces]);
    CGFloat bounciness = [self bounciness];
    
    CGFloat pow = powf(bounciness, bounces);
    CGFloat oneMinusBounciness = 1.0 - bounciness;
    
    CGFloat sumOfUnits = (1.0 - pow) / oneMinusBounciness + pow * 0.5;
    CGFloat unitAtT = t * sumOfUnits;
    
    CGFloat bounceAtT = logx(-unitAtT * (1.0 - bounciness) + 1.0, bounciness);
    CGFloat start = floorf(bounceAtT);
    CGFloat end = start + 1.0;
    
    CGFloat startTime = (1.0 - powf(bounciness, start)) / (oneMinusBounciness * sumOfUnits);
    CGFloat endTime = (1.0 - powf(bounciness, end)) / (oneMinusBounciness * sumOfUnits);
    
    CGFloat midTime = (startTime + endTime) * 0.5;
    CGFloat timeRelativeToPeak = t - midTime;
    CGFloat radius = midTime - startTime;
    CGFloat amplitude = powf(1.0 / bounciness, (bounces - start));
    
    return (-amplitude / (radius * radius)) * (timeRelativeToPeak - radius) * (timeRelativeToPeak + radius);
}

@end
