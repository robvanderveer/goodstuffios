//
//  UIView+GoodStuff.h
//  GoodStuffIOS
//
//  Created by Rob van der Veer on 3/1/15.
//  Copyright (c) 2015 Rob van der Veer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (GoodStuff)

- (UIImage *)imageByRenderingView;

@end