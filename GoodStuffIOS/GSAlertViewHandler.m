//
//  AlertHelper.m
//  SimplyStats
//
//  Created by Rob van der Veer on 1/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "GSAlertViewHandler.h"

@interface GSAlertViewHandler()
@property (nonatomic, strong) GSAlertClickedHandler alertHandler;
@property (nonatomic, strong) void (^completionBlock)(NSInteger buttonIndex);
@end

@implementation GSAlertViewHandler

//...NS_REQUIRES_NIL_TERMINATION;

-(GSAlertViewHandler *)initWithTitle:(NSString *)title message:(NSString *)message completed:(GSAlertClickedHandler)handler
{
    self = [super
            initWithTitle:title
            message:message
            delegate:self
            cancelButtonTitle:@"Cancel"
            otherButtonTitles:nil];
    if(self)
    {
        self.alertHandler = handler;
    }
    return self;
}

-(GSAlertViewHandler *)initWithTitle:(NSString *)title
                             message:(NSString *)message
                          completion:(void (^)(NSInteger buttonIndex))completion
                   cancelButtonTitle:(NSString *)cancelTitle
                   otherButtonTitles:(NSString *)otherTitles, ...
{
    va_list args;
    va_start(args, otherTitles);
    
    self = [super
            initWithTitle:title
            message:message
            delegate:self
            cancelButtonTitle:cancelTitle
            otherButtonTitles:nil];
    if(self)
    {
        for (NSString *arg = otherTitles; arg != nil; arg = va_arg(args, NSString*))
        {
            [self addButtonWithTitle:arg];
        }
        self.completionBlock = completion;
    }
    
    va_end(args);
    return self;
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (self.alertHandler) {
        self.alertHandler(buttonIndex,buttonIndex == 0);
    }
    else
    {
        if(self.completionBlock)
        {
            self.completionBlock(buttonIndex);
        }
    }
}
@end
