//
//  FadeBackgroundSegue.m
//  SimplyStats
//
//  Created by Rob van der Veer on 12/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "GSFadeBackgroundSegue.h"

@implementation GSFadeBackgroundSegue

-(void)perform
{
    UIViewController *src = (UIViewController *) self.sourceViewController;
    UIViewController *dst = (UIViewController *) self.destinationViewController;
    
    UIColor *oldColor = src.view.backgroundColor;
    UIColor *newColor = dst.view.backgroundColor;
    
    dst.view.backgroundColor = oldColor;
    
    [UIView animateWithDuration:0.4
                     animations:^{
                         src.view.backgroundColor = newColor;
                     }
                     completion:^(BOOL finished) {
                         [src.navigationController pushViewController:dst animated:YES];
                     }
     ];
    
}
@end
