//
//  GSEasingFunctionBack.m
//  GoodStuffIOS
//
//  Created by Rob van der Veer on 14/7/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "GSEasingFunctionBack.h"

@implementation GSEasingFunctionBack
-(id)init
{
    return [self initWithAmplitude:0.3];
}

-(id)initWithAmplitude:(CGFloat)amplitude
{
    self = [super init];
    if(self)
    {
        self.amplitude = amplitude;
    }
    return self;
}

-(CGFloat)easeCore:(CGFloat)t
{
    return t*t*t-t*[self amplitude]*sin(t*M_PI);
}
@end
