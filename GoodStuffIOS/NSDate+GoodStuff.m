//
//  NSDate+GoodStuff.m
//  CalendarDemo
//
//  Created by Rob van der Veer on 15/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "NSDate+GoodStuff.h"

@implementation NSDate (GoodStuff)

-(bool)isBetween:(NSDate *)from and:(NSDate *)to
{
    NSTimeInterval i = [self timeIntervalSinceReferenceDate];
    return ([from timeIntervalSinceReferenceDate] <= i &&
            [to timeIntervalSinceReferenceDate] >= i);
}

-(NSInteger)daysApartFrom:(NSDate *)fromDateTime
{
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&toDate
                 interval:NULL forDate:self];
    
    NSDateComponents *difference = [calendar components:NSDayCalendarUnit
                                               fromDate:fromDate toDate:toDate options:0];
    
    return ABS([difference day]);
}

@end
