//
//  AlertHelper.h
//  SimplyStats
//
//  Created by Rob van der Veer on 1/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void (^GSAlertClickedHandler) (NSInteger selectedIndex, BOOL didCancel);

/**
 UIAlertView implementation using a completion block instead of delegates. Pretty handy when you use multiple UIAlertViews in a single viewController.
 */
@interface GSAlertViewHandler : UIAlertView<UIAlertViewDelegate>

/**
 * Initializes an instance of GSAlertViewHandler with just a title, a message, and a cancel button.
 *
 * Example: TODO
 *
 * @param title The title for the alert
 * @param message The message body for the alert
 * @param handler The block function that will be called when the alert is dismissed.
 * @return The initialized alertview
 */
-(GSAlertViewHandler *)initWithTitle:(NSString *)title message:(NSString *)message completed:(GSAlertClickedHandler)handler;

/**
 * Initializes an instance of GSAlertViewHandler with the same parameters as a normal UIAlertView, only this version uses a completion block instead of a delegate.
 * @param title The title for the alert
 * @param message The message body for the alert
 * @param completion The block function that will be called when the alert is dismissed.
 * @param cancelTitle The title for the cancel button
 * @param otherTitles The titles for any other buttons that you need. Requires nil termination.
 * @param ... append other button titles
 * @return The initialized alertview
 */
-(GSAlertViewHandler *)initWithTitle:(NSString *)title
                             message:(NSString *)message
                          completion:(void (^)(NSInteger buttonIndex))completion
                   cancelButtonTitle:(NSString *)cancelTitle
                   otherButtonTitles:(NSString *)otherTitles, ... NS_REQUIRES_NIL_TERMINATION;
@end
