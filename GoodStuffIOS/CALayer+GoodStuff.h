//
//  CALayer+additions.h
//  SimplyStats
//
//  Created by Rob van der Veer on 7/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/CALayer.h>
#import <UIKit/UIKit.h>

/**
 CALayer extension categories
 */
@interface CALayer (GoodStuff)

/**
 * Moves the layer to be on top of other layers
 *
 * Note the side effect that the layer is removed and re-added to the sublayer structure.
 */
- (void)moveToFront;

/**
 * Returns an UIImage snapshot from the layer
 *
 * @param size The size of the generated image
 * @param opaque Use true when the layer is opaque, false if not
 * @param scale Sets the contentscale for the resulting image.
 * @return An UIImage snapshot of the layer with the given size and contentscale.
 */
- (UIImage *)getImageWithSize:(CGSize)size opaque:(bool)opaque contentScale:(CGFloat)scale;

/**
 * Returns an UIImage snapshot from the layer with default options. See getImageWithSize:
 * @return An UIImage snapshot of the layer with the default size and contentscale.
 */
- (UIImage *)getImageFromLayer;
@end