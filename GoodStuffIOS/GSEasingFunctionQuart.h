//
//  GSEasingFunctionQuart.h
//  GoodStuffIOS
//
//  Created by Rob van der Veer on 14/7/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "GSEasingFunction.h"

/**
 Represents an easing function that creates an animation that accelerates and/or decelerates using the formula f(t) = t^4.

 ![easeIn](../docs/Images/curveEasingFunctionQuart_easeIn.png =200x150)
 ![easeOut](../docs/Images/curveEasingFunctionQuart_easeOut.png =200x150)
 ![easeInOut](../docs/Images/curveEasingFunctionQuart_easeInOut.png =200x150)
 
 see GSEasingFunction
 
 */
@interface GSEasingFunctionQuart : GSEasingFunction

@end
