//
//  NSCalendar+GoodStuff.h
//  GoodStuffIOS
//
//  Created by Rob van der Veer on 23/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <Foundation/Foundation.h>

/** Calendar extension classes */
@interface NSCalendar (GoodStuff)

/** Returns the first day of a month for a given date 
 * @param date the date for which to determine the first day.
 * @returns the first day of the month
 */
-(NSDate *)firstDayOfMonthFor:(NSDate *)date;

/** Adds a number of units (weeks, days, months) to a given date
 @param number the amount of units to add
 @param units the units to add (NSDayCalendarUnit, NSMonthCalendarUnit, etc).
 @param date The date to add the units to.
 @returns the resulting date
 */
-(NSDate *)add:(NSInteger)number units:(NSUInteger)units toDate:(NSDate *)date;

/** Subtracts a number of units (weeks, days, months) to a given date
 @param number the amount of units to subtract
 @param units the units to subtract (NSDayCalendarUnit, NSMonthCalendarUnit, etc).
 @param date The date to subtract the units to.
 @returns the resulting date
 */
-(NSDate *)substract:(NSInteger)number units:(NSUInteger)units toDate:(NSDate *)date;

/** create a new day with just day, month and year units 
 @param day The day
 @param month The month
 @param year The year
 @returns A new date composed by day, month and year only.
 */
-(NSDate *)createDate:(NSInteger)day month:(NSInteger)month year:(NSInteger)year;

/** trims a date to the given precision, e.g. trimming to NSMonthCalendarUnit removes the day unit and the time 
 @param date The date to be trimmed
 @param units The precision. Do not combine flags like NSDayCalendayUnit|NSYearCalendarUnit. This will throw an exception.
 */
-(NSDate *)setPrecision:(NSDate *)date toUnits:(NSUInteger)units;
@end