//
//  RVVisualPreferences.m
//  CalendarDemo
//
//  Created by Rob van der Veer on 14/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "GSVisualPreferences.h"

@implementation GSVisualPreferences

-(id)init
{
    self = [super init];
    if(self)
    {
        self.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:16];
        self.smallFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:12];
        self.largeFont = [UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:22];
        self.lineWidth = 2;
        self.fillColor = [UIColor colorWithWhite:1.0 alpha:0.5];
        self.borderColor = [UIColor colorWithWhite:1.0 alpha:1];
        self.textColor = [UIColor colorWithWhite:0 alpha:1];
        self.disabledTextColor = [UIColor colorWithWhite:0 alpha:0.3];
        self.activeFillColor = [UIColor colorWithWhite:0 alpha:0.5];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

+(GSVisualPreferences *)standardAppearance
{
    static GSVisualPreferences *appearance;
    
    if(!appearance)
    {
        appearance = [[GSVisualPreferences alloc] init];
    }
    return appearance;
}

-(GSVisualPreferences *)initWith:(GSVisualPreferences *)prefs
{
    self = [super init];
    if(self)
    {
        self.font = prefs.font;
        self.smallFont = prefs.smallFont;
        self.largeFont = prefs.largeFont;
        self.lineWidth = prefs.lineWidth;
        self.fillColor = prefs.fillColor;
        self.borderColor = prefs.borderColor;
        self.textColor = prefs.textColor;
        self.disabledTextColor = prefs.disabledTextColor;
        self.activeFillColor = prefs.activeFillColor;
        self.backgroundColor = prefs.backgroundColor;
    }
    return self;
}

-(GSVisualPreferences *)copy
{
    GSVisualPreferences *result = [[GSVisualPreferences alloc] initWith:self];
    return result;
}
@end
