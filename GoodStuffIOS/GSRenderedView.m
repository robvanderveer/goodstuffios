//
//  GSRenderedView.m
//  GoodStuffIOS
//
//  Created by Rob van der Veer on 14/1/15.
//  Copyright (c) 2015 Rob van der Veer. All rights reserved.
//

#import "GSRenderedView.h"

@implementation GSRenderedView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        self.renderOpaque = YES;
        self.renderScaleFactor = 0;
    }
    return self;
}

-(NSBlockOperation *)renderInQueue:(NSOperationQueue *)queue completion:(void (^)(void))completed
{
    //abort and pending rendering.
    NSBlockOperation *block = [[NSBlockOperation alloc] init];
    __weak NSBlockOperation *weakBlock = block;
    
    [block addExecutionBlock:^{
        UIImage *renderedImage = [self renderToImageWithOperation:weakBlock];
        
        if([weakBlock isCancelled])
        {
            return;
        }
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            self.image = renderedImage;
            
            if(completed)
            {
                completed();
            }
        }];
        
        
    }];
    
    [queue addOperation:block];
    return block;
}

-(UIImage *)renderToImageWithOperation:(NSBlockOperation *)operation
{
    BOOL opaque = [self renderOpaque];
    CGFloat scaleFactor = [self renderScaleFactor];
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, opaque, scaleFactor);
    
    NSLog(@"drawing %fx%f @%f", self.bounds.size.width, self.bounds.size.height, scaleFactor);
    
    [self renderWithOperation:operation];
    
    UIImage *renderedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return renderedImage;
}

-(void)renderWithOperation:(NSBlockOperation *)operation
{
    //must implement.
}

@end
