//
//  NSMutableAttributedString+GoodStuff.m
//  SimplyStats
//
//  Created by Rob van der Veer on 17/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "NSMutableAttributedString+GoodStuff.h"

@implementation NSMutableAttributedString (GoodStuff)

UIFont *_normalFont;
UIFont *_boldFont;

-(void)setDefaultFont:(UIFont *)font
{
    _normalFont = font;
}

-(UIFont *)defaultFont
{
    return _normalFont;
}

-(void)setBoldFont:(UIFont *)boldFont
{
    _boldFont = boldFont;
}

-(UIFont *)boldFont
{
    return _boldFont;
}

-(void)append:(NSString *)string
{
    if(self.defaultFont)
    {
        [self append:string withFont:self.defaultFont];
    }
    else
    {
        [self appendAttributedString:[[NSAttributedString alloc] initWithString:string]];
    }
}

-(void)append:(NSString *)string withFont:(UIFont *)font;
{
    [self append:string withStyle:NSFontAttributeName value:font];
}

-(void)append:(NSString *)string withStyle:(NSString *)style value:(id)value
{
    if(string && string.length > 0)
    {
        NSAttributedString *addString = [[NSAttributedString alloc] initWithString:string attributes:@{style:value}];
        
        [self appendAttributedString:addString];
    }
}

-(void)appendBold:(NSString *)string
{
    [self append:string withFont:self.boldFont];
}

-(void)appendBoldInteger:(int)number
{
    [self appendBold:[NSString stringWithFormat:@"%d", number]];
}
@end
