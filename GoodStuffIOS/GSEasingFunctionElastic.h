//
//  GSEasingFunctionElastic.h
//  GoodStuffIOS
//
//  Created by Rob van der Veer on 14/7/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "GSEasingFunction.h"

/**
 Represents an easing function for an elastic animation. It behaves a little like the Bounce animation, with overshot.
 
 ![easeIn](../docs/Images/curveEasingFunctionElastic_easeIn.png =200x150)
 ![easeOut](../docs/Images/curveEasingFunctionElastic_easeOut.png =200x150)
 ![easeInOut](../docs/Images/curveEasingFunctionElastic_easeInOut.png =200x150)

 see GSEasingFunction
 
 */
@interface GSEasingFunctionElastic : GSEasingFunction

/** Sets or gets the springiness of the animation. Defaults to 5. */
@property (nonatomic) CGFloat springiness;

/** Sets or gets the number of oscillations. Default to 3. */
@property (nonatomic) int oscillations;

/** Initializes the instance with a specific springiness and oscillations 
 *
 * @param springiness TODO
 * @param oscillations TODO
 */
-(id)initWithSpringiness:(CGFloat)springiness andOscillations:(int)oscillations;
@end
