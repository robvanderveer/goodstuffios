//
//  RVDatePicker.m
//  CalendarDemo
//
//  Created by Rob van der Veer on 15/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "GSDatePicker.h"
#import "GSGridView.h"
#import "UIColor+GoodStuff.h"
#import "NSDate+GoodStuff.h"
#import "NSCalendar+GoodStuff.h"

@interface GSDatePicker ()
{
    UIImage *imgFillColor;
    UIImage *imgActiveFillColor;
    UIImage *imgTextColor;
}
//private properties
@property (nonatomic, strong) GSCalendarView *daysView;
@property (nonatomic, strong) UIView *monthsView;
@property (nonatomic, strong) UIView *yearsView;

@property (nonatomic, strong) UIView *visibleView;
@property (nonatomic, strong) UIButton *previousButton;
@property (nonatomic, strong) UIButton *nextButton;
@property (nonatomic, strong) UIButton *changeSelectionButton;

@end

@implementation GSDatePicker

-(void)initCommon
{
    _calendar = [NSCalendar currentCalendar];
    _selectedDate = nil;
    _mode = SelectDate;
    _maximumDate = [NSDate date];   //cannot select tomorrow for instance;
    _minimumDate = [_calendar substract:10 units:NSYearCalendarUnit toDate:[NSDate date]];

    _daysView = [[GSCalendarView alloc] init];
    _daysView.appearance = _appearance;
    _daysView.maximumDate = _maximumDate;
    _daysView.minimumDate = _minimumDate;
    _daysView.calendar = _calendar;
    _daysView.delegate = self;
    [self addSubview:_daysView];
    _visibleView = _daysView;
    
    [self createNavigationButtons];
    [self setAutoresizesSubviews:NO];
    [self setAutoresizingMask:UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initCommon];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        [self initCommon];
    }
    return self;
}

-(void)setSelectedDate:(NSDate *)selectedDate
{
    _selectedDate = selectedDate;
    if(!selectedDate)
    {
        _selectedDate = [NSDate date];
    }
    _daysView.displayDate = selectedDate;
}

-(void)layoutSubviews
{
    CGFloat w = self.bounds.size.width;
    CGFloat h = self.bounds.size.height;
    
    _previousButton.frame = CGRectMake(0, 0, 60, 30);
    _nextButton.frame = CGRectMake(w-60, 0, 60, 30);
    _changeSelectionButton.frame = CGRectMake(60, 0, w-120, 30);
    
    _visibleView.frame = CGRectMake(0, 30, w, h-30);
}

-(void)setAppearance:(GSVisualPreferences *)appearance
{
    _appearance = appearance;
    [_daysView setAppearance:appearance];
    
    imgFillColor = [self.appearance.fillColor getSolidImage];
    imgActiveFillColor = [self.appearance.activeFillColor getSolidImage];
    imgTextColor = [self.appearance.activeFillColor getSolidImage];
    
    [self createNavigationButtons];
}

-(void)didChangeSelection:(GSCalendarView *)sender newDate:(NSDate *)date
{
    //other day selected in the daysView
    if(self.delegate)
    {
        [self.delegate didChangeSelection:self newDate:date];
    }
}


-(void)selectPrevious:(id)sender
{
    [self goNextOrPrevious:false];
}

-(void)selectNext:(id)sender
{
    [self goNextOrPrevious:true];
}

-(void)goNextOrPrevious:(bool)next
{
    int amount = (next)?1:-1;
    
    switch(_mode)
    {
        case SelectDate:
            [self showDays:[[self calendar] add:amount units:NSMonthCalendarUnit toDate:_daysView.displayDate] animateLeft:!next];
            break;
        case SelectMonth:
            [self showMonth:[[self calendar] add:amount units:NSYearCalendarUnit toDate:_daysView.displayDate] animateLeft:!next];
            break;
        case SelectYear:
            [self showYear:[[self calendar] add:amount*9 units:NSYearCalendarUnit toDate:_daysView.displayDate] animateLeft:!next];
            break;
    }
}

-(void)showDays:(NSDate *)newDate animateLeft:(bool)goLeft
{
    GSCalendarView *oldView = _daysView;
    
    GSCalendarView *newView = [[GSCalendarView alloc] init];
    newView.appearance = _daysView.appearance;
    newView.minimumDate = oldView.minimumDate;
    newView.maximumDate = oldView.maximumDate;
    newView.calendar = _calendar;
    newView.delegate = self;
    newView.startDate = oldView.startDate;
    newView.endDate = oldView.endDate;
    newView.displayDate = newDate;
    
    _daysView = newView;
    _mode = SelectDate;
    [self animateReplaceView:_visibleView to:newView left:goLeft];
    [self updateMonthName];
}

-(void)showMonth:(NSDate *)newDate animateLeft:(bool)goLeft
{
    _daysView.displayDate = newDate;
    _mode = SelectMonth;
    UIView *nextView = [self createMonthsView:_daysView.displayDate];
    [self animateReplaceView:_visibleView to:nextView left:goLeft];
    [self updateMonthName];
}

-(void)showYear:(NSDate *)newDate animateLeft:(bool)goLeft
{
    _daysView.displayDate = newDate;
    _mode = SelectYear;
    UIView *nextView = [self createYearsView:newDate];
    [self updateMonthName];
    [self animateReplaceView:_visibleView to:nextView left:goLeft];
}


-(void)changeView:(id)sender
{
    //clicked button on top.
    
    switch (_mode)
    {
        case SelectDate:
            [self showMonth:_daysView.displayDate animateLeft:true];
            break;
        case SelectMonth:
            [self showYear:_daysView.displayDate animateLeft:true];
            break;
        case SelectYear:
            [self showDays:[NSDate date] animateLeft:false];
            break;
    }
}

-(void)monthSelected:(id)sender
{
    UIButton *button = (UIButton *)sender;
    NSInteger monthNumber = button.tag;
    
    //adjust the month number of the current displaydate and switch to day view.
    NSDateComponents *comp = [_calendar components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:_daysView.displayDate];
    [comp setMonth:monthNumber];
    
    NSDate *date = [_calendar dateFromComponents:comp];
    _daysView.displayDate = date;
    [self showDays:date animateLeft:false];
}

-(void)yearSelected:(id)sender
{
    UIButton *button = (UIButton *)sender;
    NSInteger yearNumber = button.tag;
    
    //adjust the month number of the current displaydate and switch to day view.
    NSDateComponents *comp = [_calendar components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:_daysView.displayDate];
    [comp setYear:yearNumber];
    
    NSDate *date = [_calendar dateFromComponents:comp];
    [self showMonth:date animateLeft:false];
}

#pragma mark -

-(void)createNavigationButtons
{
    [_previousButton removeFromSuperview];
    [_nextButton removeFromSuperview];
    [_changeSelectionButton removeFromSuperview];
    
    _previousButton = [self createButtonWithTitle:@"<<"];
    _nextButton = [self createButtonWithTitle:@">>"];
    _changeSelectionButton = [self createButtonWithTitle:@""];    //update later.
    [self addSubview:_previousButton];
    [self addSubview:_nextButton];
    [self addSubview:_changeSelectionButton];
    
    [self updateMonthName];
    
    [_previousButton addTarget:self action:@selector(selectPrevious:) forControlEvents:UIControlEventTouchUpInside];
    [_nextButton addTarget:self action:@selector(selectNext:) forControlEvents:UIControlEventTouchUpInside];
    [_changeSelectionButton addTarget:self action:@selector(changeView:) forControlEvents:UIControlEventTouchUpInside];
}

-(UIButton *)createButtonWithTitle:(NSString *)title
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:title forState:UIControlStateNormal];
    [button setBackgroundImage:imgFillColor forState:UIControlStateNormal];
    [button setTitleColor:self.appearance.textColor forState:UIControlStateNormal];
    
    //disabled
    [button setTitleColor:self.appearance.disabledTextColor forState:UIControlStateDisabled];
    
    //selected
    [button setBackgroundImage:imgActiveFillColor forState:UIControlStateSelected];
    
    //hilighted.
    [button setTitleColor:self.appearance.borderColor forState:UIControlStateHighlighted];
    [button setBackgroundImage:imgTextColor forState:UIControlStateHighlighted];
    
    //selected + hilighted
    [button setTitleColor:self.appearance.borderColor forState:UIControlStateSelected|UIControlStateHighlighted];
    [button setBackgroundImage:imgTextColor forState:UIControlStateSelected|UIControlStateHighlighted];
    
    [button.titleLabel setFont:self.appearance.largeFont];
    return button;
}

-(UIView *)createMonthsView:(NSDate *)displayDate
{
    NSDate *monthOfMinium = [_calendar firstDayOfMonthFor:_minimumDate];
    NSDate *monthOfMaximum = [_calendar firstDayOfMonthFor:_maximumDate];
    
    //construct a date with the given month.
    NSDateComponents *comps = [_calendar components:NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit fromDate:displayDate];
    
    GSGridView *newView = [[GSGridView alloc] init];
    newView.columns = 3;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setCalendar:self.calendar];
    NSArray *monthSymbols = [formatter shortMonthSymbols];
    NSInteger monthNumber = 1;
    for(NSString *monthName in monthSymbols)
    {
        UIButton *monthButton = [self createButtonWithTitle:monthName];
        //enable me if i'm the same month.
        [monthButton addTarget:self action:@selector(monthSelected:) forControlEvents:UIControlEventTouchUpInside];
        [monthButton setTag:monthNumber];
        
        //disable the month when the date is out of range.
        comps.month = monthNumber;
        NSDate *buttonDate = [_calendar dateFromComponents:comps];
        bool isInRange = [buttonDate isBetween:monthOfMinium and:monthOfMaximum];
        [monthButton setEnabled:isInRange];
        
        [newView addSubview:monthButton];
        
        monthNumber++;
    }
    
    return newView;
}


-(UIView *)createYearsView:(NSDate *)displayDate
{
    GSGridView *newView = [[GSGridView alloc] init];
    newView.columns = 3;
    
    //find the current year.
    NSDateComponents *comps = [_calendar components:NSEraCalendarUnit|NSYearCalendarUnit fromDate:displayDate];
    NSDateComponents *minimumComps = [_calendar components:NSEraCalendarUnit|NSYearCalendarUnit fromDate:_minimumDate];
    NSDateComponents *maximumComps = [_calendar components:NSEraCalendarUnit|NSYearCalendarUnit fromDate:_maximumDate];
    NSUInteger startYear = comps.year - 4;
    
    for(NSInteger year = startYear; year < startYear + 9; year++)
    {
        NSString *yearName = [NSString stringWithFormat:@"%ld", (long)year];
        UIButton *yearButton = [self createButtonWithTitle:yearName];
    
        [yearButton addTarget:self action:@selector(yearSelected:) forControlEvents:UIControlEventTouchUpInside];
        [yearButton setTag:year];
        
        bool isInRange = (minimumComps.year <= year) && (year <= maximumComps.year);
        [yearButton setEnabled:isInRange];
        
        [newView addSubview:yearButton];
    }
    
    return newView;
}

#pragma mark -
-(void)updateMonthName
{
    NSDate *referenceDate = [_calendar setPrecision:_daysView.displayDate toUnits:NSMonthCalendarUnit];
    NSDate *monthOfMinimum = [_calendar setPrecision:_minimumDate toUnits:NSMonthCalendarUnit];
    NSDate *monthOfMaximum = [_calendar setPrecision:_maximumDate toUnits:NSMonthCalendarUnit];
    
    //we can also disable/enable prev/next buttons here.
    [_previousButton setEnabled:[monthOfMinimum compare:referenceDate] == NSOrderedAscending];
    [_nextButton setEnabled:[referenceDate compare:monthOfMaximum] == NSOrderedAscending];

    NSLog(@"%@ %@ %@", _minimumDate, _daysView.displayDate, _maximumDate);
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setCalendar:self.calendar];
    
    switch (_mode) {
        case SelectDate:
            [formatter setDateFormat:@"MMMM YYYY"];
            break;
        case SelectMonth:
            [formatter setDateFormat:@"YYYY"];
            break;
        case SelectYear:
            [_changeSelectionButton setTitle:@"Today" forState:UIControlStateNormal];
            return;
    }
    
    NSString *monthName = [formatter stringFromDate:_daysView.displayDate];
    [_changeSelectionButton setTitle:monthName forState:UIControlStateNormal];
}

-(void)animateReplaceView:(UIView *)oldView to:(UIView *)newView left:(bool)left
{
    [self addSubview:newView];

    newView.frame = CGRectMake(0, oldView.frame.origin.y, oldView.frame.size.width, oldView.frame.size.height);
    newView.alpha = 0;
    
    CGFloat displacement = self.bounds.size.width;
    if(left)
    {
        displacement = -displacement;
    }
    
    CGAffineTransform t = CGAffineTransformMakeTranslation(displacement, 0);
    CGAffineTransform tOld = CGAffineTransformMakeTranslation(-displacement/2, 0);
    newView.transform = t;
    oldView.transform = CGAffineTransformIdentity;
    
    [UIView animateWithDuration:0.7 delay:0 options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         oldView.alpha = 0;
                         newView.alpha = 1;
                         
                         oldView.transform = tOld;
                         newView.transform = CGAffineTransformIdentity;
                     }
                     completion:^(BOOL finished) {
                         [oldView removeFromSuperview];
                     }];
    
    _visibleView = newView;
}
@end
