//
//  GSEasingFunctionBounce.h
//  GoodStuffIOS
//
//  Created by Rob van der Veer on 14/7/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "GSEasingFunction.h"

/**
 Represents an easing function that creates an animation that bounces towards the target.
 
 ![easeIn](../docs/Images/curveEasingFunctionBounce_easeIn.png =200x150)
 ![easeOut](../docs/Images/curveEasingFunctionBounce_easeOut.png =200x150)
 ![easeInOut](../docs/Images/curveEasingFunctionBounce_easeInOut.png =200x150)
 
 see GSEasingFunction
 
 */
@interface GSEasingFunctionBounce : GSEasingFunction

/** the number of bounces, defaults to 3 */
@property (nonatomic) int bounces;

/** the 'bounciness' of the animation. Default value is 2. Higher numbers give bigger bounces. */
@property (nonatomic) CGFloat bounciness;

/**
 * initializes with a specified number of bounces
 * @param bounces The number of bounces
 */
-(id)initWithBounces:(int)bounces;

/**
 * initializes with a specified number of bounces and a bounciness
 * @param bounces The number of bounces
 * @param bounciness The desired bounciness for the animation
 */
-(id)initWithBounces:(int)bounces andBounciness:(CGFloat)bounciness;
@end
