//
//  GSEasingFunction.m
//  GoodStuffIOS
//
//  Created by Rob van der Veer on 14/7/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "GSEasingFunction.h"

@implementation GSEasingFunction

-(id)init
{
    self = [super init];
    if(self)
    {
        self.mode = EaseIn;
    }
    return self;
}

-(CGFloat)ease:(CGFloat)time
{
    switch (_mode) {
        case EaseIn:
            return [self easeCore:time];
        case EaseOut:
            return 1 - [self easeCore:(1.0 - time)];
        case EaseInOut:
            if(time < 0.5)
            {
                return [self easeCore:time * 2] * 0.5;
            }
            else
            {
                return (1.0 - [self easeCore:(1.0 - time) * 2.0]) * 0.5 + 0.5;
            }
        default:
            @throw [[NSException alloc] initWithName:@"Argument out of range" reason:@"easing mode" userInfo:nil];
    }
}

-(CGFloat)easeCore:(CGFloat)t
{
    return 0;
}
@end
