//
//  UIDevice+GoodStuff.m
//  GoodStuffIOS
//
//  Created by Rob van der Veer on 29/7/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "UIDevice+GoodStuff.h"

@implementation UIDevice (GoodStuff)
- (BOOL) hasRetinaDisplay
{
    return ([UIScreen mainScreen].scale == 2.0f);
}
@end
