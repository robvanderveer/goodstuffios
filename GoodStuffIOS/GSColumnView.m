//
//  GSColumnView.m
//  GoodStuffIOS
//
//  Created by Rob van der Veer on 04-02-15.
//  Copyright (c) 2015 Rob van der Veer. All rights reserved.
//

#import "GSColumnView.h"

@implementation GSColumnView

-(void)layoutSubviews
{
    NSMutableArray *maxColumnsWidths = [[NSMutableArray alloc] init];

    //pass one, calculate cell widths.
    //all children are considered rows for layout.
    for(UIView *row in self.subviews)
    {
        int cellIndex = 0;
        for(UIView *cell in row.subviews)
        {
            //create a minimum column width.
            while(maxColumnsWidths.count  <= cellIndex)
            {
                [maxColumnsWidths addObject:[NSNumber numberWithFloat:0]];
            }

            //get the max for the current column.
            CGFloat columnWidth = [maxColumnsWidths[cellIndex] floatValue];

            [cell sizeToFit];

            CGSize size = cell.bounds.size;
            

            if(size.width > columnWidth)
            {
                maxColumnsWidths[cellIndex] = [NSNumber numberWithFloat:size.width];
            }

            cellIndex++;
        }
    }

    //pass two: apply the width changes to each cell.
    CGFloat yOffset = 0;
    for(UIView *row in self.subviews)
    {
        int cellIndex = 0;
        CGFloat xOffset = 0;
        CGFloat rowHeight = 0;

        for(UIView *cell in row.subviews)
        {
            if(cellIndex > 0)
            {
                xOffset += self.cellSpacing;
            }

            CGFloat columnWidth = [maxColumnsWidths[cellIndex] floatValue];

            if(cell.frame.size.height > rowHeight)
            {
                rowHeight = cell.frame.size.height;
            }

            //set the left + width of the cell.
            cell.frame = CGRectMake(xOffset, 0, columnWidth, cell.frame.size.height);
            xOffset += columnWidth;


            cellIndex++;
        }

        //size the container cell.
        row.frame = CGRectMake(0, yOffset, xOffset, rowHeight);

        yOffset += rowHeight;
    }
}

@end
