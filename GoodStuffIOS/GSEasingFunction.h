//
//  GSEasingFunction.h
//  GoodStuffIOS
//
//  Created by Rob van der Veer on 14/7/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/** The possible easing modes 
 *
 * This is the longer description.
 *
 * @available Since monday
 * @see GSEasingFunction
 */
typedef NS_ENUM(NSUInteger, GSEasingMode)
{
    /** Specifies an easing mode where the start of the animation is slowed down 
     * @available since tuesday
     * @see GSAnimation
     */
    EaseIn,
    
    /** Specifies an easing mode where the end of the animation is smoothed 
     *
     * Longer description
     */
    EaseOut,
    
    /** Specifies an easing mode where both the start and the and of the animation are smoothed */
    EaseInOut
};

/** This is an abstract base class that represents a function for generating key frame values. The function can be used the GSAnimation class to create nonstandard animation types.

  There are several easing functions to choose from:
 
 | | | |
 |:-----------------------:|:------------------------:|:------------------------:|
 |     ![Back][1]        |       ![Linear][2]     |     ![Bounce][3]       |
 | GSEasingFunctionBack  | GSEasingFunctionLinear | GSEasingFunctionBounce |
 |     ![Cubic][4]       |        ![Quart][5]     |      ![Quit][6]        |
 | GSEasingFunctionCubic |  GSEasingFunctionQuart |  GSEasingFunctionQuint |
 |    ![Elastic][7]      |                        |                        |
 | GSEasingFunctionElastic |                        |                        |
 
 
 [1]: ../docs/Images/curveEasingFunctionBack_easeIn.png =200x150
 [2]: ../docs/Images/curveEasingFunctionLinear_easeIn.png =200x150
 [3]: ../docs/Images/curveEasingFunctionBounce_easeIn.png =200x150
 [4]: ../docs/Images/curveEasingFunctionCubic_easeIn.png =200x150
 [5]: ../docs/Images/curveEasingFunctionQuart_easeIn.png =200x150
 [6]: ../docs/Images/curveEasingFunctionQuint_easeIn.png =200x150
 [7]: ../docs/Images/curveEasingFunctionElastic_easeIn.png =200x150
 
 @see GSAnimation
 */
@interface GSEasingFunction : NSObject
/** Easing mode to use
 
 Valid values for GSEasingMode are `EaseIn`, `EaseOut` and `EaseInOut`. The default value is `EaseIn`.

 */
@property (nonatomic) GSEasingMode mode;

/** returns the value for a given normalized time between 0 and 1. The returned value is corrected for the selected easing mode. 
 It uses the `easeCore` function and the easing mode to generate the correct easing curve by applying a formula:
 
 - For `easeIn` the `easeCore` value is used.
 - For `easeOut`, the `easeCore` value is negated and subtracted from 1.
 - For `easeInOut`, the `easeIn` value is used for t < 0.5, and `easeOut` for t > 0.5, thereby joining the ease in and ease out curves.
 @param t Normalized time, 0..1
 */
-(CGFloat)ease:(CGFloat)t;

/** returns the value for a given normalized time between 0 and 1 without correcting the selected easing mode.
 The function should return the value as if mode `easeIn` was selected.
 @param t Normalized time, 0..1
*/
-(CGFloat)easeCore:(CGFloat)t;
@end
