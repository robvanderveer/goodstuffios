GoodStuffIOS
============

As a little brother to GoodStuff for .Net, we proudly present GoodStuff for iOS. GoodStuff
is a compilation of welcome additions to the current core library of Cocoa for iOS. 
It contains various classes and extension methods to simplify development and to boost
productivity.

GoodStuffIOS is open source. Most of the methods found in the library were already taken
from the internet, added with a coating of experience in software development. But mostly,
this library make my life easier.

For UIView subclasses we currently have:

* GSAlertViewHandler: an `UIAlertView` that supports a block operation instead of a delegate,
* GSGridView: a simple self-animating grid,
* GSVisualPreferences: an object for centralizing UI settings,
* GSCalendarView: an calendar view for a single month,
* GSDatePicker: a consumer for the GSCalendarView to quickly pick a date,
* GSPopUp: an abstract base class for customized alerts and popups

Another proud addition is the port of several `Windows.Animation` [easing functions](GSEasingFunction) and an associated `GSAnimation` class. This animation class is a simple subclass of `CAKeyFrameAnimation` that allows you to inject an easing function. GoodStuff comes with easing functions like [Bounce](GSEasingFunctionBounce), [Elastic](GSEasingFunctionElastic), [Quart](GSEasingFunctionQuart) and [Quintic](GSEasingFunctionQuint).

Also notable is a base implementation of the many LINQ-style filtering categories for `NSArray`. C# developers will be familiar with `Single()`, `First()`, `Where()`, `Any()` etc. See NSArray(GoodStuff) for details.