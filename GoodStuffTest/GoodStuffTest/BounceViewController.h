//
//  BounceViewController.h
//  GoodStuffTest
//
//  Created by Rob van der Veer on 14/7/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BounceViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *animateView;
@property (strong, nonatomic) IBOutlet UISegmentedControl *easingTypeSegment;

- (IBAction)perform:(id)sender;
@end
