//
//  TestConcreteClass.h
//  GoodStuffTest
//
//  Created by Rob van der Veer on 28/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TestProtocol.h"

@interface TestConcreteClass : NSObject<TestProtocol>
@property CGFloat someNumber;

-initWithNumber:(CGFloat)number;
@end
