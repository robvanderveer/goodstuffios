//
//  EasingCurveView.h
//  GoodStuffTest
//
//  Created by Rob van der Veer on 22/7/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoodStuffIOS/GoodStuffIOS.h>

@interface EasingCurveView : UIView
@property (nonatomic, strong) GSEasingFunction *function;
@end
