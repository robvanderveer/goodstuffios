//
//  EasingCurveView.m
//  GoodStuffTest
//
//  Created by Rob van der Veer on 22/7/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "EasingCurveView.h"
#import <GoodStuffIOS/GoodStuffIOS.h>

@implementation EasingCurveView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


- (id)initWithFrame:(CGRect)frame andFunction:(GSEasingFunction *)function
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _function = function;
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    // Drawing code
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(context, [UIColor blackColor].CGColor);
    CGContextSetLineWidth(context, 1.0);
    CGContextSetAlpha(context, 0.5);
    
    CGFloat padding = 30;
    
    CGFloat w = self.bounds.size.width;
    CGFloat h = self.bounds.size.height;
    
    CGContextMoveToPoint(context, padding, padding);
    CGContextAddLineToPoint(context, w - padding, padding);
    CGContextMoveToPoint(context, padding, h - padding);
    CGContextAddLineToPoint(context, w - padding, h - padding);
    CGContextStrokePath(context);
 
    CGFloat w2 = self.bounds.size.width - 2 * padding;
    CGFloat h2 = self.bounds.size.height - 2 * padding;
    
    CGContextSetLineWidth(context, 2.0);
    CGContextSetAlpha(context, 1);
    CGContextMoveToPoint(context, padding, h - padding);
    for(CGFloat x = 0; x <= w2; x++)
    {
        CGContextAddLineToPoint(context, x + padding, (1-[_function ease:(x/w2)])*h2 + padding); //draw to this point
    }

    // and now draw the Path!
    CGContextStrokePath(context);
}

@end
