//
//  TestConcreteClass.m
//  GoodStuffTest
//
//  Created by Rob van der Veer on 28/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "TestConcreteClass.h"

@implementation TestConcreteClass
-(void)sayHello
{
    NSLog(@"Hello");
}
-(id)initWithNumber:(CGFloat)number
{
    self = [super init];
    if(self)
    {
        self.someNumber = number;
    }
    return self;
}
@end
