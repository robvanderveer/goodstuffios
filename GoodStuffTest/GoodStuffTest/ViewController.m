//
//  ViewController.m
//  GoodStuffTest
//
//  Created by Rob van der Veer on 28/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//
//  Inspired by http://crosbymichael.com/objective-c-linq.html

#import "ViewController.h"
#import <GoodStuffIOS/GoodStuffIOS.h>

#import "TestProtocol.h"
#import "TestConcreteClass.h"

@interface ViewController ()
@property (nonatomic, strong) GSContainer *container;
@property (nonatomic, strong) GSPopUp *myAlert;
@end


@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    
    // try container.
    _container = [[GSContainer alloc] init];
    [_container setup:@protocol(TestProtocol) with:^id(GSContainer *container) {
        return [[TestConcreteClass alloc] init];
    }];
    
    id<TestProtocol> pTest = [_container obtain:@protocol(TestProtocol)];
    [pTest sayHello];
    
    
    NSArray *items = [[NSArray alloc] initWithObjects:
                      [[TestConcreteClass alloc] initWithNumber:4],
                      [[TestConcreteClass alloc] initWithNumber:7],
                      @8,
                      @9,
                      @"Test",
                      nil];
    
    NSArray *numbers = @[@4, @-3, @99];
    
    CGFloat total = [[items ofClass:[TestConcreteClass class]] sum:^CGFloat(id currentItem) {
        return [currentItem someNumber];
    }];
    assert(total == 11);
    assert([items ofClass:[NSString class]].count == 1);
    assert([items ofClass:[NSNumber class]].count == 2);
    assert([[items ofClass:[NSString class]] any:^bool(NSString *currentItem)
    {
        NSLog(@"item = %@", currentItem);
        return [currentItem isEqualToString:@"Test"];
    }]);
    assert([items firstOrDefault] != nil);

    assert([numbers max:^CGFloat(id item)
    {
        return [item floatValue];
    }] == 99);
    
    assert([numbers min:^CGFloat(id item)
            {
                return [item floatValue];
            }] == -3);
    
    assert([numbers sum:^CGFloat(id item)
            {
                return [item floatValue];
            }] == 100);
    
    _myAlert = [[GSPopUp alloc] initWithFrame:CGRectMake(0,0,200,200)];
    UILabel *help = [[UILabel alloc] init];
    help.text = @"Error";
    [help sizeToFit];
    [_myAlert addSubview:help];
    
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] init];
    [attributeString setBoldFont:[UIFont fontWithName:@"HelveticaNeue-CondensedBlack" size:20]];
    [attributeString appendBold:@"BOLD"];
    [attributeString append:@" "];
    [attributeString appendBoldInteger:42];
}

-(void)viewWillAppear:(BOOL)animated
{
    [_myAlert showWithHandler:nil animated:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    [NSThread sleepForTimeInterval:1];
    [_myAlert hide:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
