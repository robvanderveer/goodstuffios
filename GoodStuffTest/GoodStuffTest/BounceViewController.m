//
//  BounceViewController.m
//  GoodStuffTest
//
//  Created by Rob van der Veer on 14/7/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "BounceViewController.h"
#import <GoodStuffIOS/GoodStuffIOS.h>
#import "EasingCurveView.h"

@interface BounceViewController ()
@property (nonatomic, strong) EasingCurveView *curveView;
@end

@implementation BounceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    _curveView = [[EasingCurveView alloc] initWithFrame:CGRectMake(0, 0, 200, 150)];
    _curveView.backgroundColor = [UIColor clearColor];
    [[self view] addSubview:_curveView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)perform:(id)sender
{
    GSAnimation *me = [[GSAnimation alloc] initWithPath:@"position.y"];
    me.fillMode = kCAFillModeForwards;
    me.removedOnCompletion = NO;
    //me.function = [[GSEasingFunctionBack alloc] initWithAmplitude:0.3];
    //me.function = [[GSEasingFunctionLinear alloc] init];
    //me.function = [[GSEasingFunctionBounce alloc] init];
    //me.function = [[GSEasingFunctionElastic alloc] init];
    //me.function = [[GSEasingFunctionQuart alloc] init];
    me.function = [[GSEasingFunctionCubic alloc] init];
    
    switch (_easingTypeSegment.selectedSegmentIndex)
    {
        case 0:
            me.function.mode = EaseIn;
            break;
        case 1:
            me.function.mode = EaseOut;
            break;
        case 2:
            me.function.mode = EaseInOut;
            break;
    }
    
    me.duration = 1;
    me.fromValue = [NSNumber numberWithFloat:_animateView.layer.position.y];
    me.toValue = @300.0;
    //me.autoreverses = YES;
    
    if(_curveView)
    {
        [_curveView setFunction:me.function];
        [_curveView setNeedsDisplay];
        
        [self updateCurveFile];
    }
    
    [_animateView.layer addAnimation:me forKey:nil];
}

-(void)updateCurveFile
{
    NSString *mode;
    switch (_curveView.function.mode) {
        case EaseIn:
            mode = @"easeIn";
            break;
        case EaseOut:
            mode = @"easeOut";
            break;
        case EaseInOut:
            mode = @"easeInOut";
            break;
    }
    
    NSString* pathToCreate = [NSString stringWithFormat:@"curve%@_%@.png",[_curveView.function class], mode];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:pathToCreate];

    UIImage *image = [_curveView.layer getImageWithSize:_curveView.bounds.size opaque:NO contentScale:1];
    
    NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(image)];
    [imageData writeToFile:path atomically:YES];

}
@end
