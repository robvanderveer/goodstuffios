//
//  TestProtocol.h
//  GoodStuffTest
//
//  Created by Rob van der Veer on 28/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TestProtocol <NSObject>
@required
-(void)sayHello;
@end
