//
//  ArrayExtensionsTests.m
//  GoodStuffIOS
//
//  Created by Rob van der Veer on 5/7/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "ArrayExtensionsTests.h"
#import <GoodStuffIOS/GoodStuffIOS.h>

@interface ArrayExtensionsTests()
{
    NSArray *items;
    NSArray *emptyList;
}

@end

@implementation ArrayExtensionsTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
    items = @[@"aap", @"noot", @"mies"];
    emptyList = @[];
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

-(void)testSelect
{
    NSArray *result = [items select:^id(NSString *item) {
        return [NSNumber numberWithInteger:item.length];
    }];
    
    bool ok = [result all:^bool(id item) {
        return [item isKindOfClass:[NSNumber class]];
    }];

    STAssertTrue(ok, @"should be all concreteclass instances");
    STAssertTrue(result.count == 3, @"Expected 3 items");
}

-(void)testOfClass
{
    NSArray *result = [items ofClass:[NSString class]];
    
    STAssertTrue(result.count == 3, @"Expected 3 items");
}

-(void)testOfClassEmpty
{
    NSArray *result = [items ofClass:[NSNumber class]];
    
    STAssertTrue(result.count == 0, @"Expected 0 items");
}

-(void)testFirst
{
    NSString *first = [items firstOrDefault];
    
    STAssertNotNil(first, @"Expected an item");
}

-(void)testSkip
{
    NSArray *skipped = [items skip:2];
    
    STAssertEquals(skipped.count, 1U, @"just one item expected");
    STAssertEquals(skipped[0], @"mies", @"correct item value");
}

-(void)testSkipZero
{
    NSArray *skipped = [items skip:0];
    
    STAssertEquals(skipped.count, 3U, @"just one item expected");
    STAssertEquals(skipped[0], @"aap", @"correct item value");
}

-(void)testSkipAll
{
    NSArray *skipped = [items skip:items.count];
    
    STAssertEquals(skipped.count, 0U, @"no items expected");
}

-(void)testSkipNegative
{
    NSArray *skipped = [items skip:-1];
    
    STAssertEquals(skipped.count, items.count, @"3 items expected");
}

-(void)testSkipTooMany
{
    NSArray *skipped = [items skip:5000];
    
    STAssertEquals(skipped.count, 0U, @"no items expected");
}

-(void)testTake
{
    NSArray *taken = [items take:2];
    
    STAssertEquals(taken.count, 2U, @"item count");
}

-(void)testTakeAll
{
    NSArray *taken = [items take:items.count];
    
    STAssertEquals(taken.count, items.count, @"item count");
}

-(void)testTakeWayTooMany
{
    NSArray *taken = [items take:3000];
    
    STAssertEquals(taken.count, items.count, @"item count");
}

-(void)testTakeNone
{
    NSArray *taken = [items take:0];
    
    STAssertEquals(taken.count, 0U, @"item count");
}

-(void)testTakeNegative
{
    NSArray *taken = [items take:-1];
    
    STAssertEquals(taken.count, 0U, @"item count");
}

-(void)testFirstShouldNotThrowOnEmptyArray
{
    STAssertNoThrow([emptyList firstOrDefault:^bool(id item)
    {
        return true;
    }], @"Expected not to throw on empty list");
}

-(void)testArrayAnyEmptyFails
{
    bool result = [emptyList any:^bool(id currentItem) {
        return true;
    }];
    
    STAssertFalse(result, @"Should return false for empty array");
}

-(void)testArrayAllEmptySucceeds
{
    bool result = [emptyList all:^bool(id currentItem) {
        return true;
    }];
    
    STAssertTrue(result, @"Should return true for empty array");
}

-(void)testArrayAllSucceeds
{
    NSArray *numbers = @[@1.0, @2.0, @3.0];
    
    bool result = [numbers all:^bool(NSNumber *item) {
        return [item floatValue] < 5;
    }];
    
    STAssertTrue(result, @"Should return true");
}

-(void)testArrayAllWithAFail
{
    NSArray *numbers = @[@1.0, @99.0, @3.0];
    
    bool result = [numbers all:^bool(NSNumber *item) {
        return [item floatValue] < 5;
    }];
    
    STAssertFalse(result, @"Should return false");
}

-(void)testArrayAvg
{
    NSArray *numbers = @[@2.0, @3.0, @10.0];
    
    CGFloat result = [numbers avg:^CGFloat(NSNumber *item) {
        return [item floatValue];
    }];
    
    STAssertEqualsWithAccuracy(result, 5.0f, 0.001f, @"Incorrect average %f", result);
}

-(void)shouldThrowOnAvgForEmptyCollection
{
    STAssertThrows( [emptyList avg:^CGFloat(NSNumber *item)
    {
        return [item floatValue];
    }], @"expected exception");
}


-(void)testArrayAnyFailsForMismatchedItem
{
    STAssertFalse([items any:^bool(NSString *currentItem)
    {
        return [currentItem isEqualToString:@"wim"];
    }], @"Should return false");
}

-(void)testArrayAnySucceedsForMatchingSingleItem
{
    STAssertTrue([items any:^bool(id currentItem) {
        return [currentItem isEqualToString:@"aap"];
    }], @"Should return true for predicate");
}

-(void)testArrayAnySucceedsForMatchingSingleItemLast
{
    STAssertTrue([items any:^bool(NSString *currentItem) {
        return [currentItem isEqualToString:@"mies"];
    }], @"Should return false for predicate");
}

-(void)testArrayAnySucceedsForMatchingSingleItemMiddle
{
    STAssertTrue([items any:^bool(NSString *currentItem) {
        return [currentItem isEqualToString:@"noot"];
    }], @"Should return false for predicate");
}

-(void)testRange
{
    NSArray *numbers = [NSArray range:1 to:20 with:^id(NSInteger value) {
        return [NSString stringWithFormat:@"%d", value];
    }];
    
    STAssertEquals(numbers.count, 20U, @"item count");
    STAssertEqualObjects(numbers[0], @"1", @"value");
    STAssertEqualObjects(numbers[19], @"20", @"value");
}

@end
